# Documentation Labnbook

Vous trouverez ici toute la documentation Labnbook

[[_TOC_]]

# Installation et administration système

+ Lien vers la documentation d'installation (en anglais) :
[installation](admin-sys/INSTALL.md) pour une installation locale avec configuration minimale
+ Lien vers la documentation du [Ldap / CAS](admin-sys/LDAP_CAS.md)
+ Lien vers la documentation [d'assistance et de debug](assistance_debug.md)

## Configuration avancée de LabNbook

Le fichier `.env` permet de configurer LabNbook, après chaque modifier, faire `php artisan config:cache` pour qu'elle soit appliquée

```.env
# Labnbook
# Activation de la langue
i18n=true
# Duree des token d'API et decalage d'horloge toleres
TOKEN_TTL=1800
TOKEN_LEEWAY=20
# Duree des verrous sur les labdocs
LOCKER_VALIDITY=61
# Temps de synchronisation sur les rapports et messageries
REFRESH_PERIOD=20
# Historique de la refresh period, utilise pour le calcul des temps de travail
# sur les tavleaux de bords enseignants
# Se lit : jusqu'au 16 mai 2020 le temps de rafraichissement etait 30
# depuis il vaut REFRESH_PERIOD
# peut contenir plusieurs valeurs
OLD_REFRESH_PERIOD={"2020-05-16":"30"}
# comment traiter les erreurs de CSRF, pour le moment on log uniquement
CSRF_LEVEL=log
# IPS autoriser a piloter l'export PDF correspond a l'IP locale et externe du frontend
IPS=
# Adresse ou envoyer les rapport de BUG
BUG_EMAIL_REPORT_TO=
# Mission et utilisateur utiliser pour le mode de test (page /report/test)
ID_MISSION_TEST=1012
ID_USER_TEST=6

# Laravel
# Message de mainteanance
MAINTENANCE_MESSAGE=
APP_NAME=Labnbook
# production ou local
APP_ENV=local
# Autogenere par php artisan key:generate
APP_KEY=
# Mode Debug
APP_DEBUG=true
# Debu SQL tres verbeux
LOG_SQL=false
JS_DEBUG=false
APP_URL=http://localhost
TIMEZONE=Europe/Paris

# Configuration des fichiers de LOG, voir la documentation Laravel
LOG_CHANNEL=stack
LOG_DAILY_LEVEL=warning

# Configuration de la BD

DB_CONNECTION=mysql
DB_HOST=mariadb
DB_PORT=3306
DB_DATABASE=labnbook
DB_USERNAME=labnbook
DB_PASSWORD=somepassword
```

# Utiliser les commandes LabnBook

LabnBook dispose d'un CLI permettant d'effectuer les opérations courantes. Ce CLI est accessible depuis la racine du projet
et s'utilise de la manière suivante : `./lnb command`. Les commandes disponibles sont les suivantes :

|  Commande  | Description  |
|---|---|
| artisan  | Démarre un CLI PHP artisan lié à la base de données |
| bash  |  Démarre un shell dans le conteneur `front` |
| cache | Vide les caches Laravel  |
| dev | Lance le script `refresh-dev.sh` (bientôt obsolète)  |
| doc | Génère la PHP Doc  |
| down | Stoppe LabnBook (installation via Docker)  |
| help | Affiche l'aide   |
| mysql  | Lance un CLI MySQL avec les informations du fichier .env (database, user, password)  |
| mysql_init  | Initialise la base de données avec les tables de LabNbook  |
| mysql_load_test_data  | Importation de donénes de test (écrase les données actuelles) |
| perms | Met à jour les permissions des fichiers LabnBook (Logs, vendor etc) |
| restart | Redémarre LabnBook (installation via Docker)  |
| shell   | Démarre un CLI PHP artisan tinker lié à la base de données  |
| tests  | Lance tous les tests automatiques |
| testsa | Lance tous les tests d'acceptance. Exemples : <br/> - `./lnb testsa` Tous les tests d'acceptance <br/> - `./lnb testsa maClasse` <br/> Uniquement la classe de test *maClasse* <br/>- `./lnb maClasse:maFonction` Uniquement le test *maFonction* de la classe *maClasse* |
| translate  | Met à jour les fichiers de traductions  |
| update  | Met à jour Labnbook via le script `refresh.sh` qui met à jour les dépendances, effectue les migrations, etc |
| up | Démarre LabnBook (installation via Docker)  |
| quality | Lance les tests de qualité nécessaire avant une mise en production |

# Bonnes pratiques de codage

## Règles fondamentales

- **editorconfig** : Configurer son editeur pour utiliser editor config

- **encodage UTF-8** : fonctionnement standard de tous les éditeurs modernes

- **Git** : toute circulation de code source doit se faire par Git.
  Pas de code envoyé par email, pas d'archive zip, etc.

- **sauvegarder sur le serveur** : chaque jour, ce qui a été produit doit
  être déposé sur le serveur Gitlab.
  Le code par commits, même incomplets, et le reste par commentaires et fichiers dans les tickets.

- **code redondant** : à regrouper

- **diviser pour régner** : si un code s'allonge, utiliser des fonctions intermédiaires pour le structurer.
  Privilégier les fonctions pures (paramètres d'entrée, valeur de retour, pas d'effet de bord).

- **code review** : faire relire son code, et faire valider son fonctionnement dès que possible pour éviter de s'enferrer sur une fausse route.

### Conventions et règles de sécurité

#### Nommage des URL et controlleurs

##### Convention

+ Les crochets `[]` indiquent une partie optionnnel, l'utilisation des préfixes est décrite plus bas
+ Les accolades `{object}` indiquent une partie remplacée par l'id d'un objet


| Format d'url                                | Methode appelée                              | Explication                                            |
|---------------------------------------------|----------------------------------------------|--------------------------------------------------------|
| `[/prefix]/objects`                         | `[\Prefix]ObjectController::index`           | Retourne ou affiche La liste des objets                |
| `[/prefix]/objects/doStuff`                 | `[\Prefix]ObjectController::doStuff`         | Action sur plusieurs objets ou des objets indeterminée |
| `[/prefix]/object/{object}/doSomethingElse` | `[\Prefix]ObjectController::doSomethingElse` | Action sur l'objet dont l'id est passé en URL          |
| `[/prefix]/object/{object}`                 | `[\Prefix]ObjectController::show`            | Affiche l'objet (permet de le modifier)                |

Le préfixe sert à indiquer une URL réservé à une classe d'utilisateur ou à l'API.

| Préfixe   | Groupes                                | Middlewares appliqués                           | Commentaire                                            |
|-----------|----------------------------------------|-------------------------------------------------|--------------------------------------------------------|
| `teacher` | `Teacher routes` dans `routes/web.php` | `auth,disconnectTest,teacher`                   | Réservé aux utilisateur connectés avec le rôle teacher |
| `manager` | `Manager routes` dans `routes/web.php` | `auth,disconnectTest,manager`                   | Réservé aux utilisateur connectés avec le rôle manager |
| `admin`   | `Amin routes` dans `routes/web.php`    | `auth,disconnectTest,admin`                     | Réservé aux utilisateur connectés avec le rôle admin   |
| `api`     | `routes/api.php`                       | `jwtserver` et `auth:api` pour certaines routes | Réservé aux plateformes externes enregistrées          |

##### Traduction URL en Controlleur et méthode

Supposons que je veux faire un appel ajax qui permet d'effectuer l'action "faireQuelqueChose" sur l'objet Report
+ L'url doit être `/report/<id_report>/faireQuelqueChose` ou `/report/faireQuelqueChose` si la méthode peut s'appliquer sans un report existant (par exemple pour liste ou créer)
+ Le contrôleur sera alors la méthode `faireQuelqueChose` de `\App\ReportController`
+ Si il s'agit d'une route restreinte aux enseignants, l'url sera `/teacher/report/{report}/faireQuelqueChose` et le controller `\App\Teacher\Report`

L'url doit donc permettre de savoir exactement quelle est la méthode controlleur appliquée.

En laravel :

```php
Route::get('report/{report}/faireQuelqueChose', 'ReportController@faireQuelqueChose');
```

#### Gestions des routes

Les routes (dans `routes/web.php` sont groupés selon les `middlewares` activées) en trois grands groupes :

1. Les routes sans middleware spécifique : accessible sans authentification, **ne rien ajouter dans ce groupe sans bonne raison**
2. Les routes avec le middleware `auth` uniquement : accessible à tous les utilisateur, pour la partie "étudiant" de labnbook
3. Les routes avec les middleware `auth,disconnectTest,teacher` : interface enseignant interdit aux utilisateurs étudiants et à l'utilisateur test

Lorsque l'on fait une nouvelle route, bien réfléchir à qui doit l'utiliser pour la placer au bon endroit

#### Règles de sécurité

Tous les controlleurs doivent commencer par une vérification de sécurité pour savoir si l'utilisateur peut faire l'action demandée.

À noter que le middleware `teacher` vérifie que l'utilisateur est enseignant, cela peut parfois dispenser de contrôles supplémentaires (par exemple pour lister des missions).

Le début d'un controlleur ressemble donc à cela (exemple tiré de `\App\LabdocController`):

```php
/**
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \App\Labdoc  $labdoc
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, Labdoc $labdoc)
{
    $user = Auth::user();
    abort_if($user->cannot('update', $labdoc), 403, __("Vous n'avez pas le droit de modifier ce labdoc"));
```

La méthode `cannot` va chercher la fonction `update` de la politique d'authorisation de la classe de `$labdoc`. `$labdoc` étant un `\App\Labdoc` il s'agit du fichier `\App\Policies\LabdocPolicy`.
Si cette méthode réponds `false` alors la demande sera abandonner avec une erreur 403.

**Important**: penser à toujour mettre un message sur les `abort` et `abort_if` ces message peuvent être rendu sous forme d'`alertGently` soit par le javascript pour les ajax (a traiter manuellement) soit automatiquement par Laravel pour les requetes principales.

#### Découpage MVC

Dans l'idéal il faut séparer le logiciel en 3 parties :

+ Les Modèles : c'est à dire les objets qu'on manipule et les méthodes qu'ils portent, cela correspond au namespace `\App`
+ Les Vues : comment on présente ces objets et les action effectuées sur ces derniers, cela correspond aux dossiers `resources/views`, **les vues ne doivent pas modifier l'état des objets**
+ Les Controlleurs : c'est eux qui instancient et font intéragir les objets ensemble, cela correspon au namespace `\App\Http\Controller` et tous ces sous namespace

Dans un monde parfait, une requête arrive par URL à un controlleur, qui :

1. Effectue les controles de sécurité
2. Instancie les objets et les fait interagir
3. Retourne une vue (ou du json)

Cette modélisation pouvant être parfois légèrement stricte, il existes deux types d'objets particuliers :

+ Les modèles de vues : namespace `\App\Views` permettent de modéliser la façon dont on voit un objet. C'est le cas notamment pour les rapports qui peuvent être vue par des enseignant ou étudiants selon plusieurs scopes indépendamment de l'état du rapport
+ Les processus : namespaces `\App\Processes` afin d'éviter d'alourdir les controleurs, certains processus complexes peuvent être effectué dans un fichier contennant toutes les sous fonctions nécessaire aux processus. C'est le cas notament pour la mise en équipe


### Git et branches

Une connaissance minimale de Git est nécessaire car tous les développements se font dans des branches Git.
Le dépôt Git est organisé sous la forme :

```
        ----E       111-new-feature (branche temporaire)
      C---D  \      123-fix-something (branche temporaire)
     /     \  \ 
A---B-------F--H    master
                \
                 I  production
```

Le code sur uga.labnbook.fr est toujours celui de la branche *production*.
La branche *master* est protégée, donc les développeurs ne peuvent y envoyer de commits.
Il faut passer par des branches, temporaires car supprimées lorsqu'elles sont fusionnées dans *master*.

Script bash pour nettoyer ses branches locales (celles qui ont été supprimées sur Origin)
```
git_trim_branches() {
    git remote prune origin
    git branch --merged origin/master  | grep -v "master\|main" | xargs git branch -d
}
```

#### Schéma des principales commandes git

```mermaid
graph LR
	subgraph local
	W
	S
	L
	end

	W(Espace de travail) -->|git add| S(Staging)
	S -->|git commit| L[(Dépot Local)]
	W -->|git commit -a| L
	L -->|git push| R[(Dépot distant)]
	R -->|git fetch| L
        L -->|git merge| W
	R -->|git pull| W

```

## Processus de développement

Le processus est piloté par Gitlab, notamment les tickets affichés en colonnes sur
le [board](https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/labnbook/boards),
et les *Merge Requests* qui permettent d'associer une branche Git à un ticket.

1. Partir d'un ticket Gitlab aussi précis (respecter le format des issues), petit et détaillé que possible.
  Par exemple  <https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/labnbook/issues/203>
  pour le ticket `#203` (le "#" initial permet de signaler que c'est un numéro de ticket).

	- S'attribuer ce ticket
	- le passer dans la colonne *Doing* (autre méthode, lui ajouter ce label)

2. En cas de doute sur les spécifications, demander de vive voix ou par ticket.
   Si les précisions sont données à l'oral, les ajouter ensuite au ticket.

3. Dans Git, créer une branche dont le nom commence par ce numéro de ticket.
   Par exemple, `203-fix-fitex-dependencies` ou `203-nettoie-residus`.

4. Ajouter des commits dans cette branche.
   Préférer les commits *atomiques* : petits mais complets et cohérents.

5. Publier la branche dès qu'un regard extérieur peut être utile (`git push`).

6. Quand on estime le travail prêt

    + Passer le ticket dans la colonne *To Test*
    + Créer la *Merge Request* associée dans Gitlab
        L'URL de création de la MR est donnée quand on publie la branche, et aussi sur la page web [listant les branches](https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/labnbook/branches).

7. Pour être fusionnée, une MR doit être relue et testée par une personne autre que le développeur qui choisit parmis ces 3 options :

    1. Le code semble correspondre a la spec, mais l'impact est important : la branche devra passer en speed-test, on met un message sur la MR et laisse en *To Test*
    2. Le code semble correspondre a la spec et l'impact est minimal, on fusionne en master, le ticket est fermé
    3. Le code est à revoir : l'issue repasse en *Doing* on indique au développeur ce qu'il faut corriger via l'interface de revue de code de la MR ou via le ticket

8. La branche master est fusionné en production et un `git pull` est effectué sur uga.labnbook.fr, cette étape peut être délayée pour éviter un envoie en prod à une heure de forte affluence

9. Lors de la fusion on ajoute une entrée au CHANGELOG en respectant le format indiqué dans ce fichiers

10. Avant d'envoyer en prod il faut lancer le script `prod_validate.sh` depuis la branche master pour vérifier que tout est à jour :
  + Traductions
  + Changelog
  + Tests OK

Pour aller plus loin, lire la [présentation du workflow de Gitlab](https://docs.gitlab.com/ee/workflow/gitlab_flow.html).

## Environnement de développement

Un IDE pour PHP et JavaScript est fortement recommandé.
Il faut au minimum savoir exploiter :

- la coloration syntaxique et l'indentation automatique,
- la complétion des fonctions et variables selon leur type,
- la navigation dans le code source (aller à la définition d'une fonction, etc).

En pratique, on utilise actuellement JetBrains PHPStorm,
mais tout IDE équivalent est possible : VSCode, Atom, Netbeans…

## Conventions de style

Pour des raisons historiques, le code source n'est pas uniforme.

Ce qui est impératif :

- Le code source doit être indenté.
- Les symboles (variables, fonctions) sont nommés en anglais.
- Utiliser des noms explicites, même longs, par exemple `buildRowScope()`.
- Tout nouvelle fonction (JS, PHP) ou variable globale doit avoir son bloc de documentation,
  avec notamment le type de chaque argument de fonction.

Les recommandations pour le nouveau code sont :

- utiliser [editorconfig](#editoconfig) pour configurer l'indentation
- utiliser les syntaxes PHP-doc et JS-doc, avec des blocs commençant par `/**`
- convention de nommage :
	- fonctions en camelCase, avec verbe et minuscule initiale. Exemple : `convertTextToXml(text)`
	- classes PHP en CamelCase, avec majuscule initiale
	- variables et fichiers, généralement de la forme `name_like_this`

### editorconfig

Afin d'écrire un code cohérent, merci d'installer [editorconfig](https://editorconfig.org) sur votre éditeur.

+ Vim : installer [editorconfig-vim](https://github.com/editorconfig/editorconfig-vim)
+ Netbeans
    1. Télécharger la dernière version de éditor config depuis [cette page](https://github.com/welovecoding/editorconfig-netbeans/releases) (version **nbm**)
    2. Dans Netbean, Menu Outils > Plugins > Téléchargés > Ajouter un plugin
    3. Choisir le fichier nbm
    4. Clicker "Installer" et suivre les instructions
    4. Redémarer netbeans
+ Pour les autres éditeurs, voir [la page de téléchargement de editorconfig](https://editorconfig.org/#download)


### Les opérations à valider au préalable


Avant de s'engager sur une des pistes ci-dessous, il faut que le responsable de projet la valide :

- les variables globales JS
- l'ajout de bibliothèques externes PHP et JS
- les modifications structurelles de la base de données


### Évaluation (stagiaire uniquement)

Avant la soutenance, la note de stage (sur 20) est fonction (du plus au moins important) :
- du respect des spécifications
- de la quantité de bugs à l'exécution
- Bibliothèques externes utilisées sans négociation
- Variables globales inutiles
- Code redondant
- Manque de documentation (code et tickets)
- Conventions de nommage non respectées
- Mauvaise indentation du code

## Utilisation du déboggeur 

Qu'est qu'un [déboggeur](https://fr.wikipedia.org/wiki/D%C3%A9bogueur) ?

### Débogueur JS

Tutoriel dédié au déboggeur : https://developers.google.com/web/tools/chrome-devtools/javascript

Ensemble des tutoriels DevTool : https://developers.google.com/web/tools/chrome-devtools

### Débogueur PHP (local avec Docker)

LabnBook s'execute au sein d'un container Docker. L'image de base est debian buster auquel on ajoute le déboggeur XDebug. C'est ce dernier qui va permettre à PHPStorm de créer le lien entre lui et le code executé. 

Débug with docker : https://blog.eleven-labs.com/fr/debug-run-phpunit-tests-using-docker-remote-interpreters-with-phpstorm/

Doc officielle : https://www.jetbrains.com/help/phpstorm/debugging-with-phpstorm-ultimate-guide.html?keymap=primary_default_for_gnome

Configuration PHPStorm pour XDebug :

- Open settings (File / settings )
- Menu Build Execution Deployment / Docker 
- Cliquer sur +, vérifier que connection successful s'affche en bas. 
- Menu PHP 
- CLI Interpreter, 3 petis points, +, from docker vagrant etc ..., selectionner Docker et l'image de LNB 
- Vérifier la version de PHP soit bien affiché
- Menu PHP, select "PHP 7.xx "
- Menu PHP / server
- Cliquer sur +, host = localhost, port = port de dev de LNB, cocher user path mapping
- Entrer "/var/www/labnbook" en face du dossier LNB dans la colonne Absolute path on the server

Extension XDebug : https://www.jetbrains.com/help/phpstorm/2019.2/browser-debugging-extensions.html?utm_campaign=PS&utm_content=2019.2&utm_medium=link&utm_source=product

(Tutoriel issue d'EDCampus)

## Ajax bonnes pratiques

### Appel Ajax :

Exemple simplifié à partir de l'appel de synchronisation des rapports

```javascript
$.ajax({
   method: "POST",
   url: "monUrl",
   data: {
      my_param: my_param
   }
}).done(function(data) {
  // Do something with data
}).fail(function(jqXhr, textStatus) {
   if (jqXhr.status === 401) {
        logout();
        return;
    } else if (jqXhr.status === 403) {
        if (jqXhr.responseJSON) {
            alertGently(jqXhr.responseJSON.message);
        }
        window.location.href = '/report/';
        return;
    } else {
        logError( "Erreur inconnue lors de la synchronisation : " + textStatus + " code "+jqXhr.status + " Message "+jqXhr.responseText, 'danger');
        alertGently( __("Erreur inconnue lors de la synchronisation"), "warning");
    }
});
```

### Réponse PHP

Retours multiples :

```PHP
return response()->json([
    'message' => __('un message SI c\'est pertinent'),
    'nbUpdates' => $count,
    'missions' => $mission,
    'html' => view('maSousVue')->render(),
    ],
    $code
);
```

Si `$code` vaut 200 ou n'est pas précisé le code de retour sera 200 et on ira dans le `.done`, sinon on ira dans le `.fail` avec un code d'erreur et éventuellement un message pertinent

Bien entendu cet exemple est très chargé, on ne met que les champs
nécessaire, mais les règles sont:

- Toujours séparer le code de retour de la requête du contenu JSON
- Si il y a un message à renvoyer à l’utilisateur, le mettre dans un
champ `message`
- Si il y a une sous vue, toujours la mettre dans le champ `html`
- Nommer tous les objets / compteurs et booléens retournées de manière non ambigües



# Travailler sur Labnbook avec Laravel

Cette documentation regroupe des liens et recommendations sur l'utilisation de
Laravel

## Editeur et environnement

Pour la configuration de l'éditeur et de l'environnement se reférer aux
documentations sur [coding guide](conding_guide.md) et [installation](admin-sys/INSTALL.md).

## Documentations

+ [Aide mémoire Laravel LabNbook](guide-laravel-labnbook.pdf) 
+ [Documentation laravel](https://laravel.com/docs/6.x/)
+ [Référence API Laravel](https://laravel.com/api/6.x/) Fonction de recherche douteuse, voir la section [astuces](#astuces)
+ [Laracast](https://laracasts.com/series/laravel-from-scratch-2018/episodes/1) Je recommande de voir les épisodes 2 à 9 pour comprendre les bases + au besoin des épisodes spécifiques

## Organisation du système de fichier

```
├── app                         # Dossier principal
│   ├── Console
│   ├── Exceptions              # Gestion d'erreurs
│   ├── Extensions              # Pour gérer les utilisateurs
│   ├── Helper.php
│   ├── Http
│   │   ├── Controllers         # Controllers
│   │   └── Middleware          # Middlewares supplémentaires
│   ├── Listeners               # Reaction a des evenements Laravel
│   ├── Mail                    # Pour envoyer des mails
│   ├── Policies                # Politiques de securite pour les modeles
│   ├── Processes               # Backups, Agalan, ...
│   ├── Providers
│   ├── Views                   # Modeles de vues (gestion du scope, des widgets)
│   │   ├── Policies            # Permission des modeles de vue
├── artisan                     # Interface en ligne de commande Laravel
├── bootstrap                   # Interne Laravel
├── config                      # Configuration Laravel, récupère du .env
├── database
│   ├── factories
│   ├── migrations              # Migrations Laravel, preferer les migrations "maison"
│   └── seeds
├── docker                      # Docker (déploiement "facile")
├── docker-compose.yml
├── _ide_helper.php             # Aide les IDE a "remonter" l'injection de code
├── LICENSE
├── package.json
├── package-lock.json
├── phpunit.xml
├── public
│   ├── authoring_tools         # PHP enseignant, disparaitre
│   ├── classes                 # Retro-compatibilé, disparaitra
│   ├── common                  # Retro-compatibilé, disparaitra
│   ├── css
│   ├── data                    # Traductions JS et sql structure
│   ├── dist                    # JS généré par Mithril
│   ├── images
│   ├── index.php               # Intercepteur Laravel, ne pas toucher
│   ├── js                      # JS produit par nous
│   ├── libraries               # Dépendencs JS statiques
│   ├── mix-manifest.json
│   ├── prepare_PDF.php         # Pour Puppeteer, ne peut être bougé dans Laravel
│   ├── robots.txt
│   ├── scripts                 # Scripts maisons dont migrations a utiliser plutot que celles de Laravel
│   ├── src                     # Sources Mithril
│   ├── storage -> ../storage/app/public/   #Lien vers stockage externe
│   ├── tool_copex
│   ├── tool_fitex
│   ├── tools_standalone
│   ├── tools_utilities
│   └── tool_zwibbler
├── refresh-dev.sh
├── refresh.sh
├── resources
│   ├── views                   # Vues laravel
│   └── lang                    # Traductions Laravel
├── routes                      # Routes Laravel
├── server.php
├── start_queue.sh
├── storage
│   ├── app                     # Stockage interne
│   │   ├── missions
│   │   ├── public              # Stockage externe
│   │   └── versioning
│   ├── cache
│   ├── framework
│   └── logs                    # Logs
├── tests
├── tests.sh
├── update_docker_registry.sh
├── vendor
├── webpack.config.js
├── webpack.mix.js
└── yarn.lockarn.lock
```

## Recommendations

### CLi

Utiliser au maximum la CLI `artisan` pour démarrer du code :

```bash
# Creer un modele vide
php artisan make:model MonModele
# Creer un controlleur depuis le modèle
php artisan make:controller MonModeleController -m MonModele
```

### Routes

Par convention les routes sont de la forme `/object/`, `/object/action/`, `/object/{object}` ou `/object/{object}/action` et renvoient vers `ObjectController::action` si `action` n'est pas spécifié et que un objet paramètre n'est pas donné cela correspond à `ObjectController:list`, si un objet est donnée `ObjectController:show`.

Exemple 

| Url                 | Controller           | Methode      | Explication                                  |
|---------------------|----------------------|--------------|----------------------------------------------|
| /report             | ReportController.php | list         | Liste des rapport (Ancien report_choice.php) |
| /report/1           | ReportController.php | show         | Voir le rapport numero 1                     |
| /labdoc/3/update    | LabdocController.php | update       | Modifier le labdoc 3                         |
| /trash/deleteLabdoc | TrashController.php  | deleteLabdoc | Supprimer un labdoc de la poubelle           |

### Vues

Toutes les vues doivent étendre le template `layout.labnbook` ou d'un de ses
dérivés.

#### Logout

Laravel demande à ce que le logout soit effectué via un post et non un get, il
faut donc un formulaire pour se déconnecter. Toutes les vues incluent ce
formulaire avec un script pour le soumettre, un boutton de logout ressemble donc à :

```html
<a href="" onclick="logout();" title="Se déconnecter">
```

La fonction javascript `logout` peut prendre un message d'erreur qui sera affiché après deconnexion, exemple :

```javascript
logout(__("Vous devez accepter les CGUs pour utiliser LabNbook"));
```

#### Template

Les vues utilisent le moteur blade, cependant nous n'utilisons pas tous le
language blade, pour plus de lisibilité. Nous utilisons les racourcits suivants :


```blade
@extend('layout')       # pour hériter d'un layout

@yield('body')          # Pour créer une section à remplir dans un sous layout 

@section('body')        # Pour remplir la section body
@endsection

@include('sublayout', 'param')  # Pour importer une sous vue, permet de passer des paramètres optionnes

@csrf                   # Ajouter le token CSRF dans les formulaires, obligatoire

@can('action', objet)   # Controle d'autorisation, voir plus bas

{{ ... }}                   # équivalent à <?php echo htmlescapespecialchar(...); ?>
{!! ... !!}                 # équivalent à <?php echo ...; ?>
```

## Réutiliser des composants (modale, popup de confirmation, tooltip, etc)

### Popup de confirmation 

Les popup de confirmation sont utilisé pour attirer l'attention d'un utilisateur sur une action critique (suppression, édition, etc) et lui donner des informations complémentaires.

Pour l'utiliser :

```javascript
global_confirmation_popup.openConfirmationPopup  : function (modal_title, modal_label, modal_submit_callback, modal_cancel_callback)

global_confirmation_popup.closeConfirmationPopup : function ()
```

Exemple : 
```javascript
global_confirmation_popup.openConfirmationPopup( 
  __("Ne plus être associé à cette classe"),  
  __("En cochant cette case vous confirmez vous désassocier de cette classe. Elle va disparaître de votre liste de classes"),
  function(id_user){
    deleteTeacherFromClass(id_user, true);
  }
);

global_confirmation_popup.closeConfirmationPopup();
```

![Exemple de popup de confirmation](https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/documentation-dev-public/-/raw/master/image/confirmationPopup.png)

## Authentification et authorisation

Toutes les routes doivent contenir le middleware d'authentification :

```php
Route::resource('report', 'ReportController')->middleware('auth');
```

Aditionnellement Utilise le système d'[autorisations](https://laravel.com/docs/5.8/authorization#authorizing-actions-using-policies) et en particulier des politiques d'autorisations :

Exemple pour les autorisation des rapports :

1.  Générer un fichier de politique si il n'existe pas : `php artisan make:policy reportPolicy --model report` le nom doit être `<model>Policy`
2. Supprimé toutes les fonctions non utilisées
2.  Mettre `return false` dans toutes les fonctions du fichier générer (tout interdire par défaut)
3.  Dans le controller correspondant ajouter `abort_if($u->cannot('verb', $object), 403, __("vous n'avez pas le droit verb cet object"))`, au début de chaque méthode
4. Optionnel Dans la vue pour afficher des morceaux conditionnelement `@can('update', $report)`

### Ajax

Pour écrire un Ajax qui recharge un morceau d'une page :

1. Mettre le morceau de la page dans une sous vue et l'appeller avec un `@include('_subview', ['param1'=>$param1])`
2. Ajouter une route pour l'ajax qui retourne cette sous vue avec les bon paramètres

[Pour une explications plus complète voir ce sujet stackoverflow](https://stackoverflow.com/questions/37690068/laravel-refresh-data-after-ajax)

#### CSRF

Toutes les requetes ajax envoyés par Jquery ajoutent automatiquement un jeton CSRF
dans les headers. Pour les requetes XHR non Jquery, il faut récupérer le jeton "à la main" et le mettre dans le champ `_token`.

En php on peut récupérer le jeton avec `csrf_token()` et en JS on peut le charger depuis la propriété de la page : `document.head.querySelector('meta[name="csrf-token"]').content`

L'ajout de `{!!  $helper->configureAjax()  !!}` dans une vue après le chargement de JQuery configure les Ajax pour envoyer le jeton automatiquement.

## Astuces

+ Laravel fait beaucoup de magie, pour rendre des méthodes provenant d'une classe B sur une classe A, cela peu être difficile à suivre pour les éditeurs (mauvaise complétion) et les humains. Si vous avez configuré votre éditeur correctement, vous devez avoir un fichier `_id_helper.php` à la racine de votre dossier. Ce fichier permet de retracer la provenance réel des méthodes.

## Mails

### Debug

En mode debug les mails sont dans les logs laravel

### Envoyer un mail

Envoie de mail text brute :

```php
$url = config('app.url');
$bodyText = "Bonjour,

    Un compte LabNbook vient d'être créé :
    Compte : $this->login
    Mot de passe : $pwd

{$url}

Cet e-mail est envoyé automatiquement. Les réponses ne seront pas traitées.
";
$to = $this->email;

Mail::raw($bodyText, function ($message) use ($to) {
        $message->to($to)
        ->subject("LabNbook : informations de connexion à la plateforme");
        });
```

Envoie d'un message en Markdown à partir d'une vue (emails.errors):

```php
use Illuminate\Support\Facades\Mail;
use App\Mail\MarkdownMail;

....

Mail::to($addr)->queue(new MarkdownMail('emails.errors', 'Une erreur est survenue', $data));
```

Le mail est mis en queue et traité de manière différée



## Gérer un état coté client JS 

Une problématique récurrente est de conserver un état coté client en Javascript. Par exemple dans la gestion de la liste des enseignants d'une équipe pédagogique si on veut connaître les nouveaux enseignants et les enseignants supprimés. Pour résoudre ce problème on va utiliser l'encapsulation graĉe aux fonctions JS. Ci après un exemple :

```javascript
var gloabl_employee = function () {
  var name = "Aditya Chaturvedi";
	var exp = new RegExp(/\d+/);
	return {
		setName : function (value) {
			if( exp.test(value) ) {
				alert("Invalid Name");
			}
			else {
				name = value;
			}
		},
		getName : function () {
			return name;
		}
  };  // end of the return
}(); // Note this '()' means we're calling the function and assigning the returned value to the variable global_employee

// To test
alert(global_employee.getName());   // Aditya Chaturvedi
global_employee.setName( "Rahul Khanna" );
alert(global_employee.getName());  // Rahul Khanna
global_employee.setName( 42 ); // Invalid Name; the name does'nt changes.
global_employee.name = 42;     // Doesn't affect the private fullName variable.
alert(global_employee.getName());  // Rahul Khanna is printed again.
```

La convention de nommage de tels objets est "global_...".

# Aide mémoire : LabNbook - Laravel

## Dossiers principaux 



```
app/                    # Dossier principal PHP
    Http/Controllers/   # Controllers
    Views/              # Modeles de vues (scope)
    Policies/           # Permissions
    resources/views/    # Vues laravel
routes/                 # Routes (lien url -> controller)
public/                 # js, images, css et code non Laravel
storage/app/            # Stockage applicatif interne
    public/             # Stockage accessible par url
```

Voir : https://laravel.com/docs/6.x/structure

## Traitement des requêtes 

1. Interception
2. Middlewares (Routing (URL) : https://laravel.com/docs/6.x/routing, auth,CSRF,...)
3. Controller
4. Réponses : https://laravel.com/docs/6.x/responses
5. Middlewares

## Permissions 

Chaque objet peut avoir un fichier de politique associé

* Les politiques du fichier `foo/Bar.php` sont dans  `foo/Policies/BarPolicy.php`
* On appele les politique avec la syntaxe ``` $user->cannot('verb', $obj)```
* Il existe l'opposé ```can('verb', $obj)```
* ```abort_if($cond, $code, $msg)``` interrompt le traitement de la requête

Voir : https://laravel.com/docs/6.x/authorization\#authorizing-actions-using-policies


## Models :

Générer la PHPDoc pour un model :

```
docker exec -it labnbook_front_1 php artisan ide-helper:models "App\MaClasse"
```

## Controller web

Le controlleur doit vérifier les permissions pas l'authentification, `$labdoc` est instancié depuis l'id  dans l'url
```php
public function move(Request $request, Labdoc $labdoc){
    $user = Auth::user(); // utilisateur connecte, Auth::id() pour juste l'id
    abort_if($user->cannot('update', $labdoc), 403, __("Vous n'avez pas le droit de modifier ce labdoc")); // Permission
    $id_rp = (int) $request->input('id_rp'); // Parametre de la requete (POST ou GET)
    $ans = $labdoc->move($user->id_user, $id_rp); // Methode du modele
    return response()->json($ans); } // Reponse en json
```

Voir : https://laravel.com/docs/6.x/controllers

## Templates blades (vues)

Les vues sont en PHP avec quelques aides, dont :

* `@include(view)` inclusion d'une vue
* `@extend(view)` étends une autre vue
* `@yield(nom)` cree une section vide (vue mère)
* `@section(nom)` insère dans la section (vue fille)
* `@csrf` jeton csrf a mettre dans les formulaires
* `{{ ... }}` PHP echo html echapé
* `{{ !! ... !! }}` PHP echo non echapé

Voir : https://laravel.com/docs/6.x/views

## Artisan : interface en ligne de commande

Voir : https://laravel.com/docs/6.x/artisan\#introduction

    `php artisan config:cache` : apres modif du `.env`
    `php artisan config:clear` : vide la config
    `php artisan generate:key` : Genère une nouvelle clé
    `php artisan tinker` : Ouvre un shell PHP laravel avec accès à la BBD
    `php artisan make:<stuff>` : crée un fichier laravel controller, politique...)
    `php artisan down|up` : Mode maintenance on|off
    `php artisan list` : liste les commandes


## Modèles, relations et collections : 


```php
$u = \App\User::find(1); // Equivalent a \App\User::where('id_user', 1)->first();
$reports = $u->reports; // Liste des rapports du user (en tant que learner)
$u->reports->contains(3); // Le rapport 3 est-il un des rapports du user
// Jointure avec les missions, reports() est la query des rapports
$u->reports()->join('mission', 'report.id_mission', 'mission.id_mission')->get();
// Les ld du rapport 3 et de la report_part 1 selection faire en BD
\App\Report::find(3)->labdocs()->where('id_report_part', 1)->get();
// Meme resultat mais charge les labdocs en memoire et selectionne en PHP
\App\Report::find(3)->labdocs>where('id_report_part', 1);
```

Voir https://laravel.com/docs/6.x/eloquent et https://laravel.com/docs/6.x/eloquent-collections

## Base de donnée 


```php
// SQL genere par le query builder
DB::table('labdoc_status')
    ->where('id_labdoc', $id_labdoc)
    // le deuxieme where est un AND
    ->where('id\_user', $opt, $id_user)
    // Au format [['field' => value],..]
    ->update($updates);
// SQL pur ($sql est une string parametree)
DB::insert(DB::raw($sql),
    [':id_ld' => $this->id_labdoc]);
// Retourne une Collection de StdClass
DB::select(DB::raw($sql), $params);
// Retourne une Collection de \App\User
\App\User::hydrate(
    DB::select(DB::raw($sql), $params));
// Request mixte
\App\User::where('id_inst', '<>', 1)
    ->whereRaw($sql, $params)->get();
```

Voir : https://laravel.com/docs/6.x/queries

## Internationnalisation

Inclure un texte traduit dyanmiquement (blade PHP) :

```php
__("ceci est une traduction :variable", ["variable" => $variable)])
```

Inclure un texte traduit dyanmiquement (JS) :

```js
__("ceci est une traduction {{variable}}", {variable : maMariable})
```

Pour échapper un caractère spécial type ' : 
```js
__("ceci est une traduction {{- variable}}", {variable : hTMLEscapeNoConv(maMariable)})
```

Génrer un fichier de traducation dans un git-bash : 

```sh 
bash public/scripts/genTranslationJson
  js : public/data/languages/en/translation.json
  php: resources/lang/en.json
```

## Migration/mise à jour de BDD

Tout changement de BDD (création de table, changement de structure doit être fait dans une migration). Labnbook utilise son propre système de migration et pas celui de Laravel

Pour créer une migration :  

`docker-compose exec front php public/scripts/migrate.php`

Le script va générer un fichier du format `YYYY-MM-DD_HHMM_new.sql` dans le dossier `public/scripts/migrations/`. Vous pourrez insérer vos scripts SQL dans ce fichier en remplcant "new" par une nom representatif de la migration 

## Ouvrir un terminal dans le conteneur front :

`docker-compose exec front bash`

# Synchronisation des rapports (specs XML) 

La function `\APP\Report::synchronize` permet à plusieurs étudiants d'éditer en collaboration un rapport.  `\APP\Report::synchronize` retourne un XML dont voici la spec : 

```xml
<modification>

	<deleted>
	<!-- labdocs deleted by a collaborator -->
		<ld id_ld="xxx" id_rp="xxx"/>
	</deleted>
	
	<modified>
	<!-- labdocs modified by a collaborator : contains the new names and contents -->
		<ld id_ld="xxx" id_rp="xxx">
			<name>
				{string} : name
			</name>
			<content>
				{html/xml/json} : LD content
			</content>
		</ld>
	</modified>
	
	<moved>
	<!-- labdocs moved by a collaborator : contains the new positions -->
		<ld id_ld="xxx" id_rp="xxx">
			{int} : position in RP [1..]
		</ld>
	</moved>
	
	<drafted>
	<!-- labdocs drafted by a collaborator -->
		<ld id_ld="xxx" id_rp="xxx">
			{bool} : draft status
		</ld>
	</drafted>
	
	<ld_list>
	<!-- list of all LD on the server for (1) verification purposes after modifications and (2) info icons updating -->
		<ld id_ld="xxx" id_rp="xxx">
			<status>{string} : "free_ld" / "modified_ld" / "locked_ld"</status>
			<editor>{string} : name_of_the_locker or last editor</editor>
			<last_edition>{string} : date of last edition formated</last_edition>
		</ld>
	</ld_list>
	
	<comment>
	<!-- labdocs with unread comments -->
		<ld id_ld="xxx"/>
	</comment>
	
	<message>
	<!-- conversation with unread messages : contains the last msg for refreshing the list of the conversations -->
		<conv id_conv="xxx">
			{html} : last_msg
		</conv>
	</message>
	
	<connected_user>
		<!-- collaborators currently connected to the report -->
		<user id_user="xxx"/>
	</connected_user>
	
</modification>

```

# Documentation métier Labnbook

Voir : https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/documentation-dev-public/-/blob/master/_Main_doc_LNb.docx

Mots clés de recherche : 

* Architecture du code 
  * Structure des pages PHP
  * Dossiers et fichiers spéciaux
  * Architecture générales des requêtes AJAX
  * Principaux concepts réifiés dans LabNBook
  * Principales abréviations utilisées
  * Variables de session PHP
* Page report.php
  * Variables globales JS
  * Statut des LD (à partir des données de la BD)
  * Affichage de la page report suivant le scope
  * Requêtes initiales d'affichage d'un rapport
  * Fonctions d’affichage d’un LD
* Synchronisation des pages entre différents utilisateurs
  * Synchronisation des modifications faites sur un LD
  * Fonctions de synchronisation


# Cycle de vie et statuts des rapports

Voir https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/documentation-dev-public/-/blob/master/Reports_lifecycle___status.pptx

# Traces générées lors de l'édition de LabDoc
Ouverture d'un labdoc avec édition : 

- Ouverture d'un labdoc 
    - Un EDIT_LD est tracé 
- Edition du labdoc par l'utilisateur
    - Si un synchronize à le temps de passer : 
         - Vérification du contenu du LD coté front. Si le contenu est différent l'ajax update labdoc est délenchée ce qui va sauvegarder le labdoc en BDD et ajouter une trace MODIFY_LD. On stocke une nouvelle version coté front dans `global_tab_ld[id_ld].ld_content. Pas de nouvelle version stocké coté back.
     - Fonctionnement identique si plusieurs synchronize ont le temps de passer.    
- Validation cas 1 : Le LD est synchronisé
     - Pas de MODIFY_LD car le contenu en base correspond au contenu du validateLD
     - Une nouvelle version est créée
- validation cas 2 : Le LD n'a pas été synchronizé 
     - MODIFY_LD tracé car le contenu en base ne correspond pas au contenu du validateLD
     - Une nouvelle version est créée

Ouverture d'un labdoc sans édition : 

- Ouverture d'un labdoc 
    - Un EDIT_LD est tracé 
- Validation rapide du labdoc (sans édition)
    - Pas de MODIFY_LD car le contenu en base correspond au contenu du validateLD
    - Pas de nouvelle version créée car new_version vaut faux. Le front n'a pas créé de version différente du contenu de ce LD.


# Trace LabnBook

Les traces LabNBook sont exploités par la recherche à des fins d'analyse de données. 

Enregistrer une trace :

```php
Trace::logAction(Trace::CHECK_ANNOTATION, $parameters);
```

Pour plus de détails : https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/documentation-dev-public/-/blob/master/Traces%20LabNbook.docx

# Stockage des fichiers déposés

Tous les fichiers déposés sont visibles de tous, même les utilisateurs non connectés,
mais l'URL ne peut être devinée, à moins d'une mauvaise configuration du serveur web.


 `files/missions/X/ld_img/` : images des labdocs
-----------------------------

Chemin `files/missions/<id_mission>/ld_img/<hash_contenu>.<extension>`.

Les dépôts sont tracés par la table `labdocfile` :
```
SELECT
  CONCAT('file/missions/', id_mission, '/ld_img/', HEX(filehash)) AS path_without_ext,
  id_labdoc, id_mission, FROM_UNIXTIME(uploadtime) AS upload_time
FROM labdocfile
ORDER BY uploadtime DESC LIMIT 5;
```

L'enregistrement se fait uniquement par `saveToFile()` dans `ImageExtractor.php`.

Pas de suppression, ni automatique ni manuelle,
mais lorsqu'une mission est supprimée son répertoire est suffixé par `.deleted`.

Les images de tous les labdocs d'une même mission sont dans le même répertoire.
Donc une même image utilisée par plusieurs labdocs, quelque que soient les rapports,
ne sera écrite qu'une fois, avec plusieurs enregistrements dans la table `labdocfile`.

Auparavant, ces images étaient dans le répertoire amont (`files/missions/X/`).


`files/missions/X/resources/` : ressources de la mission (enseignant)
----------------------------------

Chemin `files/missions/<id_mission>/resources/<nom_d_origine>`.

Enregistrement par `lb_mission_edit/file_upload.php`.


`files/missions/X/tinymce/` (enseignant)
--------------------------------

Chemin `files/missions/<id_mission>/tinymce/<nom_d_origine>`.

Documents (jpg ou pdf) déposés par l'enseignant via TinyMCE dans la mission.

Enregistrement par `lb_mission_edit/file_upload.php`
(code partiellement commun avec le dépôt de ressources par l'enseignant).


`files/missions/X/zw_img/` : zwibbler
-------------------------------

Chemin `files/missions/<id_mission>/zw_mg/<id_user>_<Y-m-d_h-i>.<extension>`.

Enregistrement par `tool_zwibbler/image_mgmt.php`
ou, en cas de copie, par `duplicateMission()` dans `authoring_tools/lb_missions/lb_missions_fn.php`.


`files/reports/` : ressources du rapport (étudiant)
---------------------

Chemin `files/reports/<id_report>/<id_user>_<Y-m-d_h-i>.<extension>`.

Dépôt par `widg_resource/process_add_res_doc.php`.

Pas de suppression automatique.

## Upload des fichiers 

Document présentant l'upload de fichier [Files upload.pptx](https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/documentation-dev-public/-/blob/master/Files%20upload.pptx).

# Dépot des images 

## Concepts

### Contenu des images

En HTML, le contenu d'une image est dans son attribut `src`.
La valeur de ce `src` est de deux types :

URI du fichier
:	Par exemple `src="https://a.com/b.png"`.
	Le navigateur ira alors chercher la ressource à l'URL indiquée.

DataUri
:	Par exemple `src="data:image/png;base64,iVBORw..."`.
	Le navigateur convertira les données qui sont encodées en base 64
    pour obtenir un contenu identique à celui d'un fichier.

En parallèle de ces images **extérieures** ou **autonome**,
en PHP et JavaScript il y a un troisième cas :

Blob
:	Le contenu brut du fichier d'image.
	On ne peut pas le mettre directement dans une balise HTML img,
	mais on peut le manipuler en JS avant de l'envoyer à PHP
	pour qu'il l'enregistre dans un fichier.

#### Surpoids des images en data-uri
**Base64** écrit 1 octet "en ASCII" pour 6 bits "en blob".
Une image autonome est donc *un tiers plus grosse* que son fichier d'origine.


### Opérations sur les images en JavaScript

Pour connaître la largeur intrinsèque d'une image
(indépendamment de son affichage), on utilise l'attribut `naturalWidth`.
Attention, il faut attendre que le navigateur ait chargé l'image.

Pour redimensionner une image en JavaScript,
il faut passer par le canevas (HTML5).
Il y a dans ce cas une nouvelle compression (donc pertes si JPEG).

Pour redimensionner de façon efficace et avec une bonne qualité,
on utilise la biblio [Pica](https://github.com/nodeca/pica).


## Situations de dépôts d'images


1. Étape d'un énoncé :
   image écrite dans le HTML, sans fichier.

2. Dessin :
   image envoyée par AJAX sous forme d'un fichier JPEG ou PNG.

3. Éditeur enrichi (TinyMCE).

4. **NON TRAITÉ** widget ressource, cf `widg_resource/resource.js` ligne 126.

### Cas 1 : énoncé


Le but est d'ajouter, à un emplacement donné dans le labdoc,
un bloc de HTML contenant l'image autonome (sans fichier).

1. Formulaire d'upload avec un `input[type=file]`

2. Image autonome cachée, contenu encodé en base64

	- Si (largeur ET taille du fichier) acceptables,
      cette image autonome est insérée dans le HTML.
	- Sinon, le redimensionnement commence.

3. Conversion de l'image en canvas (HTML5)

4. Redimenssionnement du canvas, avec la lib Pica

5. Conversion en image autonome (format et qualité fixés par le programme)

	- Cette image est insérée dans le HTML.


### Cas 2 : dessin

Le but est d'envoyer par AJAX le contenu brut de l'image.

1. Formulaire d'upload avec un `input[type=file]`

2. Image cachée, contenu encodé en base64

	- Si (largeur ET taille du fichier) acceptables,
	  on supprime cette image cachée,
	  on exporte le formulaire grâce à `FormData`.
	- Sinon, le redimensionnement commence.

3. Conversion de l'image en canvas (HTML5)

4. Redimenssionnement du canvas, avec la lib Pica

5. Conversion en blob (format et qualité fixés par le programme)

	- Cette image est envoyée par AJAX.

### Cas 3 : TinyMCE

Idem 1. Seule l'insertion de l'image autonome dans le HTML diffère.


# Annotations dans LabNbook

## Du côté technique

### Données

Une annotation est repérée par sa position dans la page, sous la forme &quot;labdoc n°123, premier bloc, deuxième paragraphe, caractères 20 à 30&quot;.

EN BD sont stockés :

- la position
- le texte surligné
- le contenu de l'annotation
- la catégorie de couleur

### Affichage

- Le texte surligné chez l'étudiant n'est pas comparé avec celui qu'avait surligné l'enseignant. Autrement dit, les caractères 20 à 30 du paragraphe 2 recevront par exemple l'annotation, même si le texte d'origine est depuis passé dans le paragraphe 3.
- Si la position n'est pas trouvé (par exemple, il n'y a plus de deuxième paragraphe), alors l'annotation n'est pas affichée.

## Du côté des utilisateurs enseignants

### Existant

Tout texte html peut être annoté :

- titres de RP (parties de rapport)
- titres de LD (LabDoc)
- contenus des LD textes, jeux de données ou protocoles

L'enseignant peut choisir la couleur de son annotation

L'enseignant peut modifier ou supprimer une annotation.

Lorsque l'enseignant ajoute une annotation, il lui est fourni une liste de précédentes annotations qu'il peut filtrer et sélectionner pour en réutiliser le texte. Les annotations fournies sont celles

- de même couleur
- écrites par l'enseignant
- dans la même RP (et donc de rapports liés à la même mission).

### Problèmes

++ Il semble que l'annotation d'équations ne fonctionne pas toujours bien : du côté enseignant, l'équation n'apparait pas en couleur ; du côté étudiant, l'annotation n'apparait pas.

++ Les images ne peuvent pas être annotées

\+  Aucune possibilité de formatage n'est possible : texte en couleur, en gras…

\+ Impossible de mettre des équations dans les annotations

\+ Les annotations sont segmentées par RP sans que ce soit explicite

### Pistes d'améliorations

+++ **Mettre des boutons ![](data:image/*;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAbCAIAAAB9Z3HzAAAAAXNSR0IArs4c6QAAAldJREFUSEtj/P//PwONARONzQcZP2oH8YHMiDXOfz7cv3jynHXX3hNhEIeES1Z3kYswA8OzQ1NaJxyXSGkr85JnR9KJLT6ebapJLJ1x4OaHL38J2vH3y1dhFS2gBW+P9hZVLDj56ObGusSadQ9/IusE+gMVvFmZbWls7Fiy9sEPDDlcAp/fvPnxdGe9H1AjBDiWbHwKV4zpj5Pnz/1i0I3NDkLxLx4PfTk1McU3tOaEeuX0Bh9pNrDKT/sXr7sE04Npx58/vxgYRIQVCAYTWAHIgoKV3xVFT7clIlnDpmJvpY7bDuLMRrKAIao1Xur1F6Dbgdac0WmYXpGQPWVhjhE82snPgxAfAC0Ie9jctv8TNIiaEuuvOeQkIiwgP59jswBoGptKQnuVCx9aUJDjD9wWoAQR3CaS7QBbsPiNY0EAIoggPsBuAclhBfHBMxaODzu7eqBxQMAC0uz4eW4KKJm6NazYPSlZgwMWFPh8AFFDQljdPHbwjkR4fYObFLtR5txJKZpc8CBiuL68pGzpAxzJngQ7QCY8WpxmEjrlEsOznavX3/0Di4Nr8xp7D9x/D0m/mIBEO0AGPF1V6hTWdFAwCh7Jf/7iLTxx2PH161sM9/ApWrqBgIONsYVbEXIq+vz1M97SAaMovT8vAVhwhvSe/ExcsfvjwdoSR2Nj+/rduNRjqaOA6Sc+Z8GdXxx8Ynzw1IPTnX+/fHj77RefY92Sbj8p7Kqw14NfrqzpaJ924OanHwRLSDYuKcOwwrJUR9xVAXY7CJpMkgIy0hVJ5pOYB0k2G6YBACQwnLXhYlrFAAAAAElFTkSuQmCC)
 pour attacher des cadres d'annotation plus globales au niveau :**

- d'un rapport
- d'une RP
- d'un LD

Ces annotations ajoutent un cadre éditable par TinyMCE sous le titre du rapport / de la RP / du LD

Ces annotations doivent pouvoir être supprimées ou modifiées

L'enseignant doit pouvoir rappeler le texte des anciennes annotations liées à la mission / à la RP

\+ **Afficher le titre de partie de la RP**

Au-dessus de la liste des annotations connues, ce titre rendrait plus explicite la segmentation.

Éventuellement, il pourrait devenir une liste déroulante des parties du rapport.

## Du côté des utilisateurs étudiants

### Existant

Les étudiants peuvent consulter les annotations écrites par les enseignants en passant sur les mots surlignés.

A la modification d'un LD, les annotations disparaissent, mais sont rechargées à la validation du LD.

### Problèmes

+++ La position de l'annotation peut varier et l'annotation peut même disparaitre (!!) si le texte en amont de l'annotation est modifié

++ Les annotations disparaissent lorsque le LD est en édition

\+ Impossible de supprimer une annotation lorsqu'elle a été traitée

\+ Les annotations ne sont pas visibles sur la version imprimable

### Pistes d'améliorations

+++  **Heuristique pour recaler les annotations**

Si le texte à surligner n'est pas celui attendu, on pourrait introduire une heuristique pour chercher à retrouver le passage d'origine (recherche floue). Cela ne fonctionnerait qu'avec une sélection de texte relativement importante. On ne pourra rien faire si la sélection portait sur 2 caractères ou sur une formule mathématique modifiée depuis.

Cyrille a travaillé sur ce problème. Il a des solutions.

++ **Annotations en regard**

À la façon des traitements de texte, un bloc dans la marge affiche les annotations.

Cela permettrait aussi d'afficher les annotations qui ont perdu leur cible.

Difficultés d'interface : s'il y a superposition des blocs, l'affichage perd son intérêt.

Comment afficher les annotations voisines ? Que faire sur un LD replié ?

\+ **Liste des annotations**

Un widget listerait les annotations de la page (début du texte, détail au survol).

Au clic, le rapport défile jusqu'à la zone concernée.

Une annotation qui a perdu sa cible serait grisée.

Astérisque si non-lue.

\+ **Limiter les annotations aux textes statiques**

C'est le plus facile (et c'est ce que fait l'outil Moodle, limité aux documents rendus).

On élimine tous les cas difficiles en sacrifiant les fonctionnalités.

# zwibbler

Zwibbler est un outil de dessin extérieur intégré à LabNbook.
Le code source de Zwibbler est maintenue par un prestatataire extérieur à LabNBook. Les mises à jours de Zwibbler nous sont fournis sous la forme d'une archive. Zwibbler se situe dans le dossier public/tools et le fichier zwibbler_fn_for_ln.js s'occupe d'assurer les liens avec LabNBook (ex : la liste des bibliothèques d'image disponibles).

Une mise à jour se passe de la manière suivante :
- Récupération du zip 
- Extraction
- Mise à jour du dépot Zwibbler : https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/zwibbler. Nous maintenons un dépot git des sources extraits des zips
- Copie des js et css de public/ du dépot zwibbler dans public/tool_zwibbler/
- Modification de public/tool_zwibbler/zwibbler_fn_for_lb.js pour coller aux modifications de public/index.html dans le dépôt zwibbler

L'ajout de nouvelles icones demande une rigueur de format afin d'avoir un set d'objet cohérent entre eux. Les images de bibliothèque sont des SVG créé avec Inkscape.

La bibliothèque PhyElec est composé d'image de taille multiple de 10 (en pixel) afin de pouvoir s'aligner automatiquement sur la grille. La bibliothèque a été pensé pour que les traits de connexion soit aligné avec la grille et que toutes les connexions d'icones puisse se faire facilement. Les traits (balise path) ont également l'atribut vector-effect="non-scaling-stroke" afin que l'epaisseur des traits reste constante même en cas de redimensionnement de l'objet. Certains objets ont également un fond blanc à l'intérieur de leurs formes afin de pouvoir placer l'objet sur un trait existant et avoir un résultat satisfaisant rapidement.

Inkscape rajoute un certains nombres de métadata inutiles pour LnB. Il est possible de nettoyer les fichiers SVG grâce à cet outil : https://jakearchibald.github.io/svgomg/.

Le plus simple pour rajouter une nouvelle icone est de dupliquer une icone existante similaire et de l'éditer.

# Outil d'analyse et de tests 

## Jeu d'essai et bases de données test (WIP)

Les tests automatiques sont lancés à partir d' [une base de données test](https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/labnbook/-/blob/master/tests/_data/labnbook_test.sql)

Teachers au sein des deux institutions de tests (UGA et Todai) : 
- UGA : Teacher1, Teacher2, Teacher3, Teacher4
- Todai : Teacher5, Teacher6, Teacher7, Teacher8

Students au sein de l'UGA : 
- student1 à student6

Équipes pédagogique :
- Équipe pédago chimie UGA, manager : teacher2, membre : teacher2
- Équipe pédago physique UGA, manager : teacher2, membre : teacher1, teacher2
- Équipe pédago maths UGA, manager : teacher2, membre :  teacher2 (archivé)
- Équipe pédago philosophie UGA, manager : teacher2, membre : teacher3, teacher2 
- Équipe pédago archéologie Todai, manager : teacher5, membre : teacher5, teacher6 
- Équipe pédago économie Todai, manager : teacher5, membre : teacher5, teacher6, teacher7, teacher8

TODO Décrire comment obtenir BDD prod anonymisé 

## Tests en local

1. Vérifier d'avoir la BDD : labnbook_test :  ```CREATE DATABASE labnbook_test;```

2. Vérifier d'avoir un utilisateur 'labnbook_test' avec des droits d'accès sur cette BDD (D'après les infos de .env.test)

3. Se placer dans le dossier racine de Labnbook, tous les tests ou un test précis

4. Pour lancer tous les tests``` docker exec -it labnbook_front_1 ./tests.sh --html ```

5.  Pour lancer un seul test ``` docker exec -it labnbook_front_1 ./tests.sh -f acceptance AddStudentsToClassCest:ImportFromForm  --html``` Note que la partie `<NomdeFichier:fonction>` est optionnelle, on peut lancer toute une suite avec juste `-f acceptance` ou tous les tests d'un fichier `-f acceptance AddStudentsToClassCest` le `-f`  indique de s'arrêter au premier echec

Pour plus d'information : [README test](https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/labnbook/-/blob/master/tests/README.md)

Remarque : Il est possible que certains tests échouent due à une latence d'execution. Si `report.html` montrent des "chargement en cours ..." on peut rajouter des waiting au sein du test concerné : 

```$I->waitForElementVisible("tr#class-$id td");```

## Tests automatique CI (WIP)

Tests automatiquement lancé dans Gitlab CI

## Sonar (WIP)

C'est un outil d'analyse statique du code source. Ils fonctionnent avec deux outils en parralèle : 
- Sonar scanner : partie analyse du code
- Sonar Hub : consultation du rapport d'analyse généré. Par commodité nous utilons un sonar disponible sur Internet

Page web sonar cloud https://sonarcloud.io/

### Utiliser Sonar 

1. Ajouter un fichier sonar-project.properties à son project
```
sonar.projectKey=labnbook
sonar.organization=labnbook

sonar.host.url=https://sonarcloud.io

sonar.exclusions=public/libraries/**, node_modules/**
```

2. Génerer un token sur sonarcloud

3. Run Sonar scanner à la racine de labnbook

```
docker run --rm -e SONAR_LOGIN="votreTokenDIdentification" -v $(pwd):/usr/src sonarsource/sonar-scanner-cli
```

4. Consulter les résultats https://sonarcloud.io/organizations/labnbook



