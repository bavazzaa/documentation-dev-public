# Assistance et debug sur LabNbook

Vous trouverez ici des procédures afin d'aider des utilisateurs en détresse ou de debuger LabNbook.

[[_TOC_]]

# Cas d'usage

## Problèmes d'installation

Vous avez installé LabNbook en local sur un serveur mais n'arrive pas a y accéder.

1. Relire [la documentation d'installation](admin-sys/INSTALL.md) et vous assurer que vous avez bien suivi la procédure en entier, en particulier
    + Peuplage de la base de donnée
    + Création et initialisation du fichier `.env`
    + Création d'une clé artisan `php artisan key:generate`
2. Si vous utilisez docker : lancer `docker-compose down && docker-compose up` pour tout relancer

```mermaid
graph TD

home{Avez vous<br />accés à<br />la page<br />de connexion?}
home -->|non| Erreur{Voyez vous un<br />message d'erreur?}
Erreur -->|non| web{Votre serveur web<br/>est-il accessible<br />et opérationnel ?}
web -->|non| configweb[Vérifier la<br />configuration de<br />votre serveur web]
web -->|oui| autofix[Lancer<br />./lnb perms<br />./lnb update<br />./lnb cache]
autofix -->|erreur| Erreur
autofix --> fixed
configweb -->fixed
Erreur -->|oui| Lire{Le message donne-t-il<br>une information claire ?}
Lire -->|non| aide[Cherchez sur<br />internet ou<br />demandez de<br />l'aide à<br />l'équipe LNB]
aide -->fixed
Lire -->|oui| corriger[Faites les<br />corrections<br />demandées]
corriger -->fixed
home -->|oui| login{Arrivez vous à<br />vous identifier ?}
login -->|non| mdp{Est-ce un problème<br />de mot de passe ?}
mdp -->|oui| defaut[Cherchez les login<br />dans la table user<br />le mot de passe<br />par défaut est mdp]
defaut -->fixed
mdp -->|non| DB[Vérifier que<br />la BD est<br />opérationnelle,<br /> accessible au<br />serveur web,<br />que les<br />identifiants dans<br />le .env<br />sont corrects]
DB -->fixed
login -->|oui| admin[Identifiez vous<br />en admin, allez<br />sur la page<br />/admin<br />vérifiez qu'il<br />n'y ait pas<br/> de problèmes<br />d'installation <br />détectés et<br />faites les<br />corrections<br />proposées]
admin -->fixed{Le problème<br />est-il réglé ?}
fixed -->|non| ko[Toutes les<br />modifications du<br />.env demandent<br />./lnb cache<br /> pour être<br />prise en compte<br />voir un <br />docker-compose down<br />docker-compose up<br />si vous<br />utilisez docker]
ko --> autofix
fixed -->|oui| ok(Content de<br />vous avoir<br />aidé !)
```

## Demande utilisateur

Un·e utilisateur·ice vous remonte un problème, voici un protocole pour aider.

### Avant de lire le flowchart

+ La première question "La demande est-elle claire" signifie, êtes vous sûr d'avoir compris
    1. Ce qu'essaye de faire l'utilisateur (son objectif)
    2. Ce qu'il fait réellement (sa manipulation)
    3. Le résultat obtenu
+ En vous connectant en admin vous pouvez
    + Via la page `Admin`
        + Voir les rapports, missions, classes etc. d'un utilisateur
        + Éditer un utilisateur (coordonnées rôles)
        + Usurper l'identité d'un utilisateur
            + Le but est uniquement de voir le problème
            + Penser à ouvrir la console avant de faire quoi que ce soir
            + N'oubliez pas : "With great power comes great responsibility"
    + Via la pages `Rapports`
        + Voir et rétablir les rapports supprimés

#### Problèmes fréquents

+ Un étudiant ne trouve plus son rapport : vérifier qu'il n'a pas été supprimé
+ Un utilisateur n'arrive pas à se connecter
    + Lui dire de demander une récupération de mot de passe
    + Si il ne reçoit pas de mail
        + Soit il a un compte CAS et a du voir un message lui disant de se connecter avec Agalan
        + Soit son adresse email est mauvaise, corriger via page d'admin
+ Un enseignant ne trouve pas un utilisateur
    + Chercher en écriture approchée sur la page admin  (typos a l'inscription)
    + Dire de confirmer les logins Agalan / numéro étudiants
+ Un problème se produisant sur le PC utilisateur mais impossible à reproduire avec une usurpation est probablement lié au PC (cache, version du navigateur)

#### Utiliser la console

Lorsque vous essayez de reproduire un bug, ouvrir la console du navigateur (F12).

Noter (captures d'écran) tous les messages qui apparaissent en rouge.

Vous pouvez voir les codes d'erreur des requêtes faite au serveur, si c'est vert avec 200 ou 300 c'est normal, si vous voyez du 400,401,403,404,419,422 ou 500,501 etc. faire une capture d'écran.

**TODO** mettre des captures d'écran de choses intéressantes.

### Flowchart


```mermaid
graph TD

demande{La demande<br />est-elle<br />claire ?}
demande -->|non| preciser[contacter<br />l'utilisateur<br />pour préciser<br />la demande ou<br />reproduire en<br />direct]
demande -->|oui| objectif{L'objectif de<br />l'utilisateur<br />est-il<br />faisable ?}

preciser --> objectif
objectif -->|non| autres{Cet objectif<br />peut-il<br />concerner d'autres<br />personnes ?}
objectif -->|oui| manipulation{L'utilisateur<br/>fait-il la<br/>bonne<br />manipulation ?}

autres -->|non| alternative[Proposer<br />une solution<br/>alternative]
autres -->|peut-être| ticket
manipulation -->|oui| resultat{Le resultat<br />est-il celui<br />attendu ?}

manipulation -->|non| assistance[Expliquer la <br />bonne manipulation]


resultat -->|non| reproduire{Arrivez vous à <br/>reproduire<br />le problème,<br />avec votre<br />compte<br />ou sur<br />labnbook-test?}
resultat -->|oui| ok[Il n'y a pas<br />de problème]


reproduire -->|non| admin[Connecter vous<br />en admin<br/>aller sur la<br />page /admin<br />regarder les<br/>classes, missions,<br />rapports de<br />l'utilisateur]

admin --> probleme{Voyez vous<br />une explication<br />au problème ?}
probleme -->|oui| reproduire
probleme -->|non| impersonate[Utiliser la <br />l'usurpation<br />pour essayer<br />de voir l'erreur]
impersonate --> reproduire2{Voyez vous<br />le problème ?}

reproduire2 -->|oui| probleme
reproduire2 -->|non| preciser
reproduire -->|oui| resoudre{Savez vous<br />résoudre le problème}
resoudre -->|oui| proposer[Proposer une solution,<br/>si possible<br />laisser l'utilisateur<br />corriger son problème]
resoudre -->|non| ticket[Ouvrir un ticket sur Gitlab]
proposer --- invis(" ")
invis -->validation{Le problème<br />était-il<br />due à LNB ?}
assistance --> validation

validation -->|peut-être| ticket
validation -->|non| fin[Problème réglé]

classDef SkipLevel width:0px,text-align:center;
class invis SkipLevel
```

## Deboggage


### À partir d'une demande utilisateur

À ce stade, on considère que l'on sait soit reproduire 
Dans un premier temps, il faut obtenir au moins un des éléments suivants 

1. Une trace d'erreur précise
2. Une succession d'opération qui permet de reproduire l'erreur autrement que avec le compte de l'utilisateur avec au moins une personne de l'équipe qui a réussit à le faire
3. Un message d'erreur lié à une page, une action ou un ajax

```mermaid
graph TD

reproduire{Savez vous<br />reproduire<br />le problème ?}
reproduire -->|non| message{Avez-vous<br />un message ou<br/>code d'erreur<br />associé à une<br />page ou une<br />action ?}
message -->|non| trace{Avez-vous une<br />une trace<br />d'erreur}
logs -->|oui| protoTrace
trace -->|oui| protoTrace[Voir le protocole<br />debug avec trace]
message -->|oui| chercher[Chercher dans le code<br />à partir du message<br />ou de la classe d'erreur<br />essayer de comprendre quelle<br />instruction peut déclencher<br />l'erreur et dans quels cas]
reproduire -->|oui| pasApas[Executez le code<br />pas a pas pour<br />comprendre où est<br />le problème]
trace -->|non| logs{Trouvez-vous<br />quelque chose<br />dans les logs ?}
logs -->|non| live[Reproduisez en<br />direct avec l'utilisateur]
live -->reproduire
```

### À partir d'une stack trace (PHP ou JS)

On considère ici que vous avez soit une stack trace soit quelque chose qui vous permet d'être certaine de la ligne de code qui déclenche l'erreur

**Note** : par cas d'utilisation "normal" on considère un cas d'utilisation qui peut arriver, même si c'est extrêment rare et improbable, à l'inverse d'un cas qui ne devrait jamais arriver mais qui se produit à cause d'un bug préalable.

```mermaid
graph TD

identifier[Identifier la ligne qui déclenche l'erreur] --> comprendre{Comprenez vous<br />comment on en arrive là}
comprendre -->|oui| normal{Est-ce un cas<br />d'utilisation normal ?}

normal -->|non| gardeFou{Pouvez-vous <br />mettre un<br />garde fous<br />pour l'éviter sans<br />casser d'autres<br />cas d'utilisation?}
normal -->|oui| silence{L'erreur devrait-elle<br />être ignorée<br />ou signalée}

silence -->|signalée| signal[Mettre un<br />alertGently ou<br />confirmPopup<br /> selon les besoins]
silence -->|ignorée| ignore[Faire en sorte<br />de continuer<br />l'execution en<br />corrigeant ou<br />ignorant l'erreur]
gardeFou -->|oui| bloquer[Faites-en sorte de<br />ne pas en arriver là<br />cela peut être <br/>révélateur d'un<br />problème d'UI / UX]
gardeFou -->|non| ignore
comprendre -->|non| ignore
ignore --> log[Au besoin<br />logger l'erreur pour<br />quantifier son importance]
```

## Remarques concernant les stack trace et les logs

### Fichiers de logs

Sur un serveur debian standard, vous trouverez 3 types de fichiers de log différents

+ Logs applicatifs dans `storage/logs`
    + `laravel-yyy-mm-dd.log` : logs PHP du jour, contient les message loggés explicitement et trace d'erreur
    + `js_errors.log` : contient les erreur javascript non intercepté qui on été remontée au serveur **attention** ce fichier contient beaucoup de bruit
    + `mysql_errors.log` : trace mysql
    + `invalid_content.log` : problème de labdocs incorrects
    + `tinymce_errors.log` : erreur propres à tinymce
+ Logs apache, requiert de se connecter à un compte sudoer sur le serveur et regarder dans `/var/log/apache2/`:
    + `labnbook_ssl_access.log`, au besoin regarder les anciens logs (`labnbook_ssl_access.log.1`, `labnbook_ssl_access.log.2.gz` etc) : liste des requêtes émise avec code de retour, permet de trouver des erreurs 4xx
    + `labnbook_ssl_error.log`, erreurs php non interceptées par Laravel, ne devrait rien contenir sauf gros plantage
+ Logs mysql, requiert de se connecter à un compte sudoer sur le serveur et regarder dans `/var/log/mysql/`:
    + `error.log` : erreurs mysql
    + `mariadb-slow.log` : requêtes lentes mysql

### Stack trace JS

Attention le fichier `js_errors.log` peut contenir beaucoup de bruit, il est à lire avec précautions.

Chaque entrée du fichier `js_errors.log` indique une date et un utilisateur, l'url `POST /log/jserror` vous indique que l'erreur a été remontée par le navigateur vers le serveur et est peu pertinente.

Les premières ligne ne correspondant pas à des fichiers dans `/libraries` vous indiquent où se situe l'erreur dans le code LabNbook.

### Remontée de bug automatique ou stack trace PHP

Les remontées de bug Automatique donnent une stack trace complète, ainsi que des infos contextuels :

+ id_user
+ Code d'erreur
+ Message d'erreur

#### Lecture de la stack trace

+ La première ligne indique l'erreur, en général elle est dans du code Laravel
+ à priori vous pouvez ignorer toutes les lignes concenrnant `vendor`
+ les deux ou trois premières lignes commençant par `/var/www/labnbook/app` vous indiquent la suite d'appel de fonction ayant conduit à l'erreur

De là vous devez pouvoir identifier le bug
