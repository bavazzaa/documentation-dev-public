# LDAP et CAS

Ce document présente la gestion du CAS et du Ldap sur LabNbook

[[_TOC_]]

## Principe général

LabNbook peut être connexté à un Central Authentication Service (CAS), et un Lightweight Directory Access Protocol (LDAP).

Le premier sert à authentifier des utilisateurs, le second à obtenir des informations sur des utilisateurs.

### Cas d'utilisation

Cette section présente la façon dont LabNbook intéragit avec le CAS et le Ldap :

1. Connexion par CAS
```mermaid
sequenceDiagram
    Note right of Utilisateur: Page de connexion<br />sur LabNbook
    Utilisateur ->>+LabNbook: Connexion par CAS
    LabNbook ->>+CAS: Authentification
    opt si l'utilisateur n'est pas déjà connecté au CAS
    CAS ->>+Utilisateur: Demande de connexion
    Utilisateur ->>-CAS: Connexion
    end
    CAS ->>-LabNbook: Connecté : <login>

    alt <login> est un compte LabNbook qui est lié au CAS
    LabNbook ->>Utilisateur: Connecté
    else a
    LabNbook ->>+Ldap: getUserByUid(<login>)
    Ldap ->>-LabNbook: <user>
        alt <user> est étudiant
        LabNbook ->>Utilisateur: Non connecté
        else <user> est enseignant
        LabNbook ->>-Utilisateur: Créer un compte ou<br />utiliser un compte existant ?
        alt créer un compte
        Utilisateur ->>+LabNbook: Créer un compte
        Note right of LabNbook: création d'un compte<br />à partir des informations<br />du LDAP
        LabNbook ->>-Utilisateur: Connecté
        else utiliser un compte existant
        Utilisateur ->>+LabNbook: Utiliser un compte existant
        LabNbook ->>-Utilisateur: Connecter vous
        Utilisateur ->>+LabNbook: Connexion LNB
        Note right of LabNbook: Mise à jour du compte<br />à partir des informations<br />du LDAP
        LabNbook ->>-Utilisateur: Connecté, lié au CAS
        end
        end
    end
```
2. Transformation d'un compte existant en compte CAS
```mermaid
sequenceDiagram
    Note right of Utilisateur: Page de connexion<br />sur LabNbook
    Utilisateur ->>+LabNbook: Connexion
    LabNbook ->>-Utilisateur: Connecté
    
    Note right of Utilisateur: Page de gestion du compte<br />sur LabNbook
    Utilisateur ->>+LabNbook: Passer en CAS
    LabNbook ->>+CAS: Authentification

    opt
        CAS ->>+Utilisateur: Demande de connexion
        Utilisateur ->>-CAS: Connexion
    end

    CAS ->>-LabNbook: Connecté : <login>
    LabNbook ->>+Ldap: getByUid(<login>)
    Ldap ->>-LabNbook: <user>
    Note right of LabNbook: Mise à jour du compte<br />à partir des informations<br />du LDAP
    LabNbook ->>-Utilisateur: Connecté, lié au CAS
```
3. Création de compte étudiant par numéro
```mermaid
sequenceDiagram
    Note right of Enseignant: page /teacher/students<br />ajout d'étudiants dans une classe
    Enseignant ->>+LabNbook: Ajout étudiant par numéro
    LabNbook ->>+Ldap: getStudentsByNumbers(<numeros>)
    Ldap ->>-LabNbook: [<users>]
    LabNbook ->>-Enseignant: Liste d'utilisateurs <br />login, prénom, nom, mail, numero etu
    Note right of Enseignant: Peut supprimer des étudiants
    Enseignant ->>+LabNbook: Ajout
    LabNbook ->>+Ldap: getStudentsByNumbers(<numeros>)
    Ldap ->>-LabNbook: [<users>]
    Note right of LabNbook: Creation des comptes<br />à partir des informations<br />du LDAP
    LabNbook ->>-Enseignant: Comptes crées

    Note right of Étudiant: Étudiant dont le compte<br />viens d'être crée

    Étudiant ->>+LabNbook: Connexion CAS
    LabNbook ->>+CAS: Authentification

    opt
        CAS ->>+Étudiant: Demande de connexion
        Étudiant ->>-CAS: Connexion
    end

    CAS ->>-LabNbook: Connecté : <login>
    LabNbook ->>-Étudiant: Connecté
```


## Configuration

La configuration du CAS et du Ldap se fait via trois composantes :

+ Deux classes php `Directory` et `LdapUser` pour modéliser la représentation des utilisateurs et la recherche par numéro étudiant dans votre `Ldap`
+ La base de donnée pour indiquer les institutions liées au Ldap et dans quelle classe inscrire les utilisateurs automatiquement inscrits
+ Le fichier `.env` pour les informations de connexion et l'instanciation du `Directory`


### Représentation du Ldap et de ses utilisateurs via les class `Directory` et `LdapUser`

Afin de pouvoir ajuster finement la représentation en interne des utilisateurs dans votre `Ldap`, en particulier la recherche par numéro étudiant et ce qui différencie une étudiante d'une enseignante, il est nécessaire d'implémenter deux classes `php`.

#### LdapUser

Afin de modéliser vos utilisateurs, vous devez créer dans `\App\Processes\Ldap` une classe qui étends et implémente la classe abstraite `LdapUser`:

```php
/** @var string unique ID (login) */
public $uid;

/**
 * Returns the users first name
 * @return string
 */
abstract public function getFirstName();

/**
 * Returns the users last name
 * @return string
 */
abstract public function getLastName();

/**
 * Returns the users email
 * @return string
 */
abstract public function getEmail();

/**
 * Return the APOGEE identifier (student number, only digits) from this LDAP record.
 *
 * @param array $numbers (opt) Preferred numbers
 * @return string
 */
abstract public function getApogeeId($numbers = []);


/**
 * Get the name of the user's institution
 * @return string
 */
abstract public function getInstitutionName(): string;

/**
 * Get the id of the user's institution in LabNbook db
 * @return int
 */
abstract public function getInstitutionId(): int;



/**
 * Checks if the ldapUser is a teacher
 * @return boolean
 */
abstract public function isTeacher();
```

L'implémentation de référence est la classe `AgalanUser` pour le Ldap de l'UGA.

#### Directory

Afin d'interoger votre `Ldap`, filtrer les résultat et instancier des `LdapUser` vous devez implémenter une classe dans `\App\Processes\Ldap` qui étends et implémente la classe abstraite `Directory` :

```php
/**
 * This class defines the required methods to implement a custom ldap directory
 */

/** @var \Symfony\Component\Ldap\Ldap */
protected $ldap;

public function __construct($ldap)
{
    $this->ldap = $ldap;
}

/**
 * Returns a LdapUser for the given uid
 * @param string $uid
 * @return \App\Processes\LdapUser|null
 */
abstract public function getUserByUid($uid);

/**
 * Query the LDAP about this student number
 * @param string $number
 * @return \App\Processes\LdapUser|null
 */
abstract public function getStudentByNumber($number);

/**
 * Query the LDAP for all the students matching these student numbers
 *
 * @param array $numbers
 * @return LdapUser|null
 */
abstract public function getStudentsByNumbers($numbers);

/**
 * Returns a LdapUser from its attributes (deserialzation)
 * @param array $attributes
 * @return \App\Processes\LdapUser
 */
abstract public function getLdapUser($attributes);
```

L'implémentation de référence est la classe `Agalan` pour le Ldap de l'UGA.

### Configuration des institution et classes en base de donnée

Dans LabNbook, le Ldap n'est accessible qu'a certaines institutions.
De plus, lorsqu'un enseignant se connecte pour la première fois via le CAS, un compte est crée automatiquement. Ce compte **doit** être associé à une institution et à une classe (au sens groupe d'enseignement).

Afin d'activer le CAS / Ldap pour une institution, il faut donc indiquer dans la table mysql `institution` l'`id_class_cas` de la classe dans laquelle on veut inscrire les enseignants.

```sql
UPDATE institution SET id_class_cas=xxx WHERE id_inst=YYY
```

Pour fixer l'`id_class_cas` d'une institution, la classe doit déjà éxister dans l'institution, il faut donc procéder en trois temps :

```sql
-- 1 creer une institution
INSERT INTO institution VALUES (1, 'UGA', NULL);
-- 2 creer une classe dans cette institution
INSERT INTO `class` VALUES (1,1,'classe auto-inscription enseignants',NULL,'2021-05-05 12:00:00','2021-05-05 12:00:00','normal', null, null);
-- 3 activer le CAS pour ce couple
UPDATE institution SET id_class_cas = 1 WHERE id_inst = 1;
```

### Configuration de la connexion via le fichier `.env`

Dans le `.env` les variables suivantes permettent de configurer les connexion CAS/ LDAP

```.env
# Chemin vers le logo a afficher sur la page de Connexion
CAS_LOGO_FILEPATH="images/logo-uga.svg"
# Noms d'affichage pour les messages concernant le CAS
CAS_NAME=AGALAN
CAS_INSTITUTION_NAME="UGA / G-INP"
# Classe utilisée comme Directory, doubler les '\'
LDAP_DIRECTORY_CLASS="\\App\\Processes\\Ldap\\Agalan"

# Parametre du CAS voir config/cas.php
CAS_HOSTNAME=
# Pour le debug du CAS
CAS_DEBUG=false
#CAS_DEBUG=/var/www/labnbook/storage/logs/cas.log
CAS_CONTEXT=''
CAS_PORT=443
CAS_CERT=''

# Parametre LDAP voir config/ldap.php
LDAP_HOST=
LDAP_PORT=636
LDAP_ENCRYPTION=ssl
LDAP_CREDENTIALS_BIND=
LDAP_CREDENTIALS_PASSWORD=
# Pour le mode Developpement
LDAP_MASQUERADE=false
```

Après avoir modifier le `.env`, ne pas oublier de faire `php artisan config:cache`

## Configuration de test

En développement on peut vouloir configurer le CAS et LDAP Masquerade, dans le `.env`

```.env
CAS_HOSTNAME=test-CAS
CAS_MASQUERADE=user-CAS
LDAP_MASQUERADE=true
LDAP_DIRECTORY_CLASS="\\App\\Processes\\Ldap\\Agalan"
```

La connexion CAS connectera toujours l'utilisateur ayant pour login `user-CAS`, ce dernier doit être présent de votre base de donnée.

Le Ldap utilisaera la configration de Masquerade voir `config/ldap.php` qui imite le LDAP de l'UGA.

