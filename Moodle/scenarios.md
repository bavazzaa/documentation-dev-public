Scenarios d'utilisation de LabNbook dans Moodle
===============================================

Si modification d'une mission L utilisée par plusieurs cours,
avertir préalablement.
Idem quand on choisit une mission déjà utilisée,
en proposant la duplication.

Pour un enseignant-éditeur
--------------------------

### Création d'une activité LabNbook (mission existante)

L'enseignant…

1. Il insère une nouvelle activité de type LabNbook.
	- plugin de type activité, alias `mod`
2. Il choisit une mission parmi celles auxquelles il a accès.
	- API de LnB de type `/api/1.0/teacher/missions?id=1`
	- système d'authentification de Moodle auprès de LnB (token ou challenge)
3. Les paramètres standards "Titre" et "Description" sont pré-remplis avec les données de LnB.
	- Si l'enseignant modifie un de ces champs, la modification est locale, nonpropagée dans LnB.
4. Les paramètres standards concernant les "groupings" permettent de segmenter par classe.
5. Une page de config spécifique permet de définir la mise en équipe (aléatoire, pré-déterminée, libre)
	- Prévoir une page de config par groupe-classe
	- Le stockage de la configuration des équipes et des équipes se fera dans des tables du plugin Moodle.
	- Cette interface est dans Moodle parce qu'elle utilise les données de Moodle (utilisateurs et groupes)
	- Que faire en cas de modification ultérieure des inscriptions aux cours ?
	  Permettre la modification sur cette page ?
	  À voir plus tard.
	- Faut-il garder des réglages de dates de début et fin alors qu'ils sont présents au niveau du cours ?
	  Oui, réglage par défaut au niveau du groupe, et au niveau  de l'équipe.
6. Retour à la page du cours, ou consultation de la vue pour enseignant de l'activité

### Création d'une activité LabNbook (mission nouvelle)

L'enseignant…

1. Insère une nouvelle activité de type LabNbook.
2. Choisit de créer une nouvelle mission (bouton → nouvel onglet)
3. Sur la page de création de mission LnB, la validation renvoie dans Moodle
	- Que faire si la session Moodle a expiré pendant la modif hors Moodle ?
	  Rien.
	- Quelle communication entre la page Moodle (JS) et LabNbook (client JS ou serveur PHP) ?
	  Aucune. Bouton de gestion des missions : duplication, modif, archivage, etc.
	- Si la création de cours est dans une page distincte, on ne peut pas fermer automatiquement l'onglet.
	  Afficher un message pour revenir à l'autre onglet ou fermer celui-ci.
	- Comment seront ensuite gérées les permissions ?
      Par exemple, est-ce que la mission créée est attribuée à l'enseignant qui l'a créée ou à tous ceux du cours ?
	  Répercuter les permissions M sur L.
	  Par exemple, L contiendrait une table d'appartenance des missions aux cours,
	  et les permissions d'un enseignants seraient déduites de cela (contexte de cours).

### Consultation d'une activité

- lien/onglet pour la gestion des équipes
- lien vers la modification de la mission
	- La page de modification de la mission doit savoir quels seront les cours M et les groupes impactés.
	- Quelles permissions si la mission est utilisée dans plusieurs cours ou par différents enseignants ?
	  Pas de restriction, un avertissement.

### Propagation des permissions

- Un enseignant a créé une mission dans L.
- Il administre un cours M avec un collègues. Tous deux sont enseignants-éditeurs (perm M).
- Il peut sélectionner et modifier la mission dont il est propriétaire selon L.
- ¡ Attention ! Il faut que son collègue puisse aussi la modifier.
- Au changement d'année, le cours est dupliqué, et l'ancien est archivé.
- ¡ Attention ! Les deux enseignants doivent pouvoir continuer à modifier la mission (avec avertissement) ou à la dupliquer.

Conséquence :
les missions accessibles à un enseignants sont celles dont il est propriétaire selon LabNbook
et celles qui sont utilisées par des cours où il enseigna.

### Reste à traiter

- Quelle intégration dans le système de complétion individuelle d'activité ?
- Quelle intégration dans le système d'évaluation-notation individuelle ?


Pour un étudiant
----------------

L'étudiant affiche la page d'accueil du cours,
où l'activité LabNbook est présentée par son titre (et sa description).
C'est le fonctionnement normal de Moodle.

### Première connexion, équipes pré-déterminées et aléatoires

- S'il n'est pas dans une équipe, signalement à l'enseignant (automatique) et avertissement à l'étudiant.
- S'il est dans une équipe, ouverture du raport en iframe ou en onglet ?
  Éventuellement hybride : iframe par défaut, avec un bouton "Pleine page dans un nouvel onglet".

Si équipes aléatoires, améliorer le système (plus tard).

### Première connexion, équipes libres

- Interface de choix d'équipe comme actuellement, mais implémentée dans Moodle.

