Intégration de LabNbook à Moodle
================================

**Ce qui suit est en grande partie obsolète.**

Principes
---------

- Un cours Moodle de format LabNbook pointe vers une mission de LabNbook (unique).
- *(à discuter)* La réciproque est fausse : plusieurs cours peuvent pointer vers la même mission.
  L'avantage est que cela permet de garder des rapports à dates différentes sur une même mission L.
  L'inconvénient est que la synchro est plus difficile
  (par ex, 2 cours et une mission doivent avoir la même description).
- L'accès aux cours (et donc aux missions et rapports) est géré par Moodle
  (inscriptions avec un rôle dans un contexte).

Les données suivantes sont équivalentes (divergence interdite) :

- date de début, fin du cours M et dates de début, fin des rapports L
- description du cours M et de la mission L
- visibilité cours et mission

Un format de cours créé pour LabNbook permettra :

- de stocker dans Moodle la liste des équipes
- d'afficher dans la zone centrale de M une interface ad-hoc pour les enseignants,
  et une autre pour les étudiants.


Interface pour l'enseignant
---------------------------

- Page normale d'un cours (paramètres du cours, inscriptions, etc).
- En zone centrale, au lieu de la saisie des sections, ressources et activités,
  mettre une interface de gestion des rapports :
	- paramétrage de la libre mise en équipes
	- équipes actuelles
	- création manuelle d'équipes
- Les actions sur un rapport :
	- afficher : nouvelle page hors Moodle
	- supprimer : impacte M et L
	- changer le status : interface locale, impacte M et L

Les équipes sont donc formées avec les étudiants inscrits au cours M.
Il faudra un mécanisme de synchro bidirectionnelle entre les équipes M et L.


Interface pour l'étudiant
-------------------------

Quand il rentre dans un cours LabNbook, la zone centrale est une iframe sur LabNbook :

- soit la mise en équipe si configurée par l'enseignant
- soit son rapport s'il existe
- soit un message disant qu'il n'a pas de rapport en cours, et que son enseignant doit en créer un
  (prévoir un signalement manuel ou automatique à l'enseignant dans ce cas).

NB : il y a 3 technos possibles pour l'intégration :

1. injecter le HTML dans la page,
2. injecter une frame/iframe,
3. envoyer sur un nouvel onglet hors M.

La solution (1) demanderait une réécriture complète (conflits JS, etc).
La solution (3) pose des problèmes d'interface (les outils de M ne sont plus accessibles)
 et des difficultés techniques (échanges M-L plus difficiles).
L'inconvénient de (2) est que les pages seront plus lourdes (colonnes M + rapport L).
Éventuellement, prévoir un mode "pleine page" pour ce format de cours.



Conséquences sur l'existant
---------------------------

1. Abandonner la messagerie : celle de Moodle la remplacera.
   NB : ceci ne s'applique que si les rapports sont dans des iframe ;
   pour des onglets séparés, il faudrait conserver la messagerie de L mais initialisée avec la config M.

2. Toute l'interface des enseignants est à refaire, dans Moodle.
   Par exemple, pour autoriser l'import il faut tenir compte des permissions dans Moodle.

3. La modification de la config d'un rapport est sans doute à proscrire (par ex, les dates sont au niveau du cours).
   Pour simplifier, au moins dans un premier temps, on peut laisser le stockage
   de certains paramètres dans leurs emplacements actuels.

4. Certaines opérations L devront aussi envoyer des données vers M.
   Par exemple, la modification d'une mission ou le changement d'état d'un raport.

5. Beaucoup d'opérations L liront leur données initiales dans M.


Problèmes et questions
----------------------

1. Si un étudiant est retiré d'un cours M :
	- on le supprime de son équipe,
	- faut-il supprimer l'équipe si elle n'a plus de membres ?

2. Gestion des groupes-groupements M en correspondance avec les équipes L ?

3. Comment relier les fichiers de Moodle et de LabNbook ?
   A minima, cela fonctionnera de façon totalement séparée.

4. Quel mécanisme de communication entre M et L ?
   Échanges XHR-HTTP en JS, container-frame en JS, curl-HTTP en PHP ?
   Système de tokens permettant à chaque appli de s'authentifier auprès de l'autre puis de signer les messages ?

