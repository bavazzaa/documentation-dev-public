Sommaire
========

[[_TOC_]]

API LabNbook pour Moodle
========================

On a choisi le protocole JWT avec un chiffrement symétrique en HS256.
La même clé est donc utilisée pour chiffrer et déchiffrer un message donné.
Par contre, cette clé varie au cours des échanges, une fois l'initialisation passée.

- M et L partagent une clé secrète appelée notée **CI** par la suite.
- Une institution est crée pour M sur L ; elle sert à stocker la clé CI.
- L'utilisateur humain est noté H.

Le chemin de l'API est noté "/v1/auth/login" avec "v1" la version actuelle de l'API.
En pratique, l'installation pourrait se faire dans un sous-domaine,
par exemple `https://api.lnb.fr/v1/auth/login` au lieu de `https://lnb.fr/api/v1/auth/login`.


Échanges en tant qu'institution
-------------------------------

Dans toutes les requêtes envoyées par M, la *payload* du JWT contient :

+ `orig: "inst"`
+ `iss: <id_institution>` : id institution dans L (stocké aussi dans la config du plugin M)
+ `sub: <id_user_ext>` : id utilisateur dans M
+ `iat: <issued at>` : timestamp UTC de l'envoi
+ `dest` : chemin dans l'API, par exemple `/v1/auth/login`
+ `data` : seul champ facultatif, dont le contenu dépend de l'API interrogée

Ce jeton JWT est signé avec la clé CI et placé dans le header `Authorization: Bearer`.
Par exemple `Authorization: Bearer eyJ0eXAiOiJKV1QiLC...`.

### Tentative de login

1. M contacte L en `POST /v1/auth/login`.
2. L répond (selon le cas):
    - `200, {"token":<KEY>, "token_exp":<timestamp>}` où `<KEY>` est une clé utilisable jusqu'à `<timestamp>` pour s'identifier comme l'utilisateur demandé
    - `401, {"message":<msg>}` : si le JWT ne peut être vérifié, `<msg>` précise le problème
    - `403, {"message":<msg>}` : si aucun utilisateur n'est lié au couple `(id_user_ext,id_institution)`

### Demande de lien utilisateur

Dans le cas 403 "Unknown user" de "/auth/login",
1. M redirige l'utilisateur en GET sur `/v1/redirect`,
  avec dans le champ `token` un jeton JWT contenant les paramètres :
    + `data:`
        + `path: /user/preBind`
        + `forward:`
            + `role_ext: <role>`: `teacher` ou `learner`
            + `return_to: <return_url>`: url où rediriger l'utilisateur après connexion
            + `login` : `username` Moodle
            + `user_name` : `lastname` Moodle
            + `first_name` : `fistname` Moodle
            + `email` : `email` Moodle
2. H voit la page de preBind LabNbook qui propose deux boutons : "J'ai déjà un compte" (cas 1) / "Créer un compte" (cas 2) et indique si il y a déjà des comptes homonymes

#### Cas 1 H clique sur "J'ai déjà un compte"

1. H clique sur le bouton et L répond à H en le revoyant sur la page de connexion L.
2. H remplit le formulaire de connexion et le soumet à L.
3. Une fois l'authentification réussie, L effectue une redirection (302) vers l'url spécifiée par M.
4. M se chargera alors de redemander l'authentification.

**Note** Si H se connecte par CAS pour la première fois à l'étape 2, il refait un tour par preBind pour fusionner le compte CAS avec un compte existant.

#### Cas 2 H clique sur "Créer un compte"

1. H Clique sur créer un compte et L redirige sur `/usr/createFromAPI`
2. L effectue une redirection (302) vers l'url spécifiée par M après avoir crée et lié un compte


Échanges en tant qu'utilisateur
-------------------------------

Pour tous les échanges, M ajoute dans le header `authorization bearer` un jeton JWT signé avec la clé `KEY` reçue précédement.
Les champs suivants sont obligatoires :

+ `iss: <id_institution>` L'id_instituion de M dans L
+ `sub: <id_user_ext>` : L'id l'utilisateur externe (dans M)
+ `iat: <issued at>` timestamp de l'heure d'envoi
+ `dest: <path>` API path, par exemple `/auth/login`
+ `orig: "user"`
+ `token_exp: <ts>` le timestamp d'expiration de la clé envoyé par L dans la poignée de main

Le champ suivant est facultatif et dépend du chemin dans l'API :

+ `data: ...` données JSON à transmettre dans la requête

Toutes les réponses sont au format :

- `200, { "data": {...}, "auth": {"token":<KEY>, "token_exp":<timestamp>}}`
- `<code>, {message: <message>}` en cas d'erreur


### En enseignant ou étudiant

- `GET /v1/redirect`  
  Redirige l'utilisateur vers une page interne de LabNbook, avec une session utilisateur si authentifié par l'API
    * entrée : `{ path: "/report.php?id_r=14", forward: {...} }` où le contenu de forward dépend du chemin
    * sortie : redirection HTTP 302

En tant qu'étudiant une fois le lien fait, on utilise toujours redirect vers `/report/enter/<id_team_config>` en passant le `groupid` effectif de l'étudiant.
Cela fait entrer l'étudiant sur un rapport pour le team_config :
    + Si `groupid` n'est pas `null` : on cherche un teamconfig correspondant au même `course` mais avec le `groupid` correspondant pour le Moodle, si il n'existe pas il est crée par duplication
    + Si l'étudiant à déjà un rapport pour ce teamconfig il entre dedans
    + Sinon on crée un rapport selon la méthode choisie dans le teamconfig, cette opération peut résulter en une erreur 400 (méthode au choix de l'enseignant mais pas de rapport) auquel cas l'étudiant est redirigé sur `/report` avec un message d'erreur

### En tant qu'enseignant

- `GET /v1/mission/`  
  Retourne la liste des missions pour lesquelles l'utilisateur est enseignant.
    * entrée : ø
    * sortie : `[ {id_mission:1, ...} ]`

- `POST /v1/mission/{id_mission}/use`  
  Crée une mise en équipe pour cette mission.
  Si aucune classe de L ne correspond au cours, cette action la crée.
    * entrée : `{ courseid: 1, coursename: <txt>, team_config: {...} }`
    * sortie : `{ team_config: {id_team_config: 4, ...} }`

- `POST /v1/mission/{id_mission}/update`  
  Modifie la mise en équipe déjà enregistrée et répercute à tous les team_config de même mission et de classe étant des sous groupes de cet classe.
    * entrée : `{ courseid: 1, team_config: {...}, id_team_config: 4 }`
    * sortie : `{ team_config: {...} }`

- `POST /v1/mission/grant`  
  Pour chacune des `team_config` désignées, donne accès à l'enseignant.
  En pratique, l'utilisateur est ajouté comme *tuteur* de chaque mission (si nécessaire) et comme enseignant de chaque classe.
  Le but est qu'un enseignant dans un cours M ait accès à toutes les activités LabNbook du cours.
    * entrée : `{ id_team_configs: [1, 2, 3] }`
    * sortie : ø

- `GET /v1/class`  
  Liste les classes L non-vides rattachées à un cours M avec pour chacune les groupes associés (et 0 pour le cours entier ?).
  Le but est de permettre à l'enseignant de copier le teamconfig d'une classe existant dans ce cours.
  Comme le choix Moodle du groupe est en pied de page, il faudra sans doute dupliquer l'affichage au niveau du choix de la méthode "copie".
    - entrée : `{ courseid: 6 }`
    - sortie : `[ {class:{id_class:1, ...}, groups: [0, 44]}, ... ]`


Changements de base de données côté L
-------------------------------------

+ `institution.key VARCHAR(256)` : clé CI
+ Ajout d'une table `link_ext_user_inst` :
    + `id_inst INT` reférence `institution.id_inst`
    + `id_user UNSIGNED INT` référence `user.id_user`
    + `id_user_ext VARCHAR(256)` identifiant sur le provider externe

