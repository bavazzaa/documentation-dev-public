Migration de LnB vers une intégration à Moodle
==============================================

Utilisateurs : correspondances M - L
------------------------------------

Comment permettre aux utilisateurs actuels de LabNbook
de passer par Moodle pour accéder à leurs contenus ?

### Correspondance à la volée

1. Un utilisateur se connecte dans Moodle.
2. Il veut accéder à un contenu LabNbook.
   Par exemple, liste de missions pour un enseignant,
   ou rapport/équipe pour un étudiant.
3. L'API de LabNbook doit permettre d'identifier autant que possible,
   parmi les utilisateurs des institutions liées au Moodle.
   Requête de serveur à serveur contenant :
   - "ID" si déjà connu de moodle
   - ou "CAS/LDAP username" ? couplé à une institution ? ssi configuré dans le plugin Moodle ?
   - ou "student ID number" ? idem ?
4. En cas d'échec à l'étape précédente,
   prévoir une authentification dans LabNbook depuis le client,
   via dialogue modal + AJAX/LabNbook ?
   Avec en alternative "Créer un compte" ?
5. En cas de succès aux étapes 4 ou 5,
   le plugin Moodle disposera d'un jeton pour l'API de LabNbook,
   avec un accès restreint à cet unique utilisateur.

Si l'identification dans LnB se fait à la volée,
alors il faudra la faire systématiquement (lenteur).
Sinon, on risque de créer des comptes doublons dans LnB,
où les missions et rapports existants ne seront pas visibles.
Prévoir une option dans le plugin Moodle :
"rattacher les comptes institutionnels M à leurs équivalents dans LnB".

### Correspondance à la migration

Pour faciliter l'utilisation dans le Moodle de l'UGA,
il faudrait éviter autant que possible ces authentifications à la volée.

Comment faire ?
Utiliser les classes ? les infos autres que login-CAS et studentID ?

### Moodle multiples

Prévoir qu'un LabNbook peut être utilisé par plusieurs Moodle.

La table `users_external` pourraît être :

- `id` PK
- `id_user` FK
- `id_institution` FK
- `external_user_id`
- `time_created`
- `time_updated`
- unique (`id_user`)
- unique (`id_institution`, `external_user_id`)


### Autres questions


