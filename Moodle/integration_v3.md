Intégration de LabNbook à Moodle
================================

Le but est de garder un minimum d'infos dans Moodle,
et de les synchroniser vers Labnbook (et uniquement dans ce sens).

Le module reste structuré sur la correspondance entre activité et mission.


Répartition entre Moodle et LabNbbok
------------------------------------

Dans Moodle :

- formulaire de création d'activité (notamment le choix d'une mission LnB)
- configuration de la mise en équipe
- synchronisation des inscrits vers les classes de LabNbook
- interrogation de l'API pour déterminer le rapport d'un étudiant
- stockage des correspondances (user M, user L)
- stockage des correspondances (cours M, groupe M, classe L)
- stockage du "teaming", avec id de son pendant dans LabNbook

Dans LabNbook :

- table pour le "teaming", c-à-d configuration des équipes pour un couple (classe, mission)
- les classes et teamings venant de Moodle sont en lecture seule
- refonte de l'interface de création de rapports
- API pour la synchronisation groupe → classe (avec création des comptes d'utilisateurs)
- API pour trouver l'ID d'un rapport pour un utilisateur de M dans une activité



"teaming" = configuration de la mise en équipe
---------

Dans Moodle, on pourra attribuer à un couple (groupe, mission)
un mode de mise en équipe :

1. aléatoire
2. au choix de l'étudiant
3. au choix de l'enseignant

Un ensemble de paramètres complètent chaque choix de méthode
(cf table `team_config` dans le LnB actuel).

Ces réglages `teaming` sont créés dans Moodle au niveau des activités,
et synchronisés vers LnB.


"teaming" dans LabNbook
-----------------------

Il faut instaurer ce mécanisme avant qu'il puisse être alimenté par le module de Moodle.

Les rapports créés ainsi seront ensuite liés à un enregistrement de "teaming".

On gardera la possibilité de créer des rapports limités à quelques utilisateurs,
sans traiter la classe complète, mais ce sera probablement rarement utilisé.

### Interface envisagée

- Bouton unique de mise en équipe, visible quand une classe est surlignée,

- Ce bouton déclenche un choix entre les possibilités :

    1. toute la classe : équipes aléatoires
    2. toute la classe : au choix de l'étudiant
    3. toute la classe : au choix de l'enseignant
    4. créer une équipe sur mesure
    5. Reprendre la mise en équipe d'un couple (cette classe, autre mission)

Les inscriptions venant de Moodle seront toujours exhaustives sur une classe,
autrement dit, tous les étudiants de la classe seront répartis en équipes (cas 1, 2 et 3).
Donc, même quand l'enseignant crée lui-même les équipes (cas 3),
LabNbook enregistrera la correspondance (classe, mission) et ses paramètres.

Par contre, depuis l'interface d'enseignant de LabNbook, on garde la possibilité
de créer une équipe à partir d'une sélection d'étudiants (cas 4), sans lien avec une classe.

Le cas 3 sera une nouveauté dans l'interface LabNbook.

Le cas 5 reprendra les équipes, et la configuration commune si elle existe.

#### Interfaces pour l'utilisateur

- Dans le cas 4, on utilise le formulaire actuel ?
- Dans les cas 1, 2, 3, le choix est suivi d'un formulaire de paramètres.
- Dans le cas 3, suivi d'un glissé-déposé d'une colonne "étudiants" vers une colonne "équipes".

#### Questions

1. Modifier une config de teaming modifiera les équipes associées ?
   Actuellement, ce n'est pas possible, car il n'y a pas de lien entre l'équipe et sa config d'origine.

2. En cas de choix d'un teaming de classe, que faire des rapports existants ?
   C'est important pour la transition vers ce nouveau système.
   Les intégrer comme le fait actuellement la mise en équipe par les étudiants ?
   Cela impliquerait de leur créer un lien vers le teaming à la création de ce dernier.
   Alternatives : les ignaorer, ou les signaler en demandant à l'enseignant que faire.


Activité, teaming et groupes dans Moodle
-----------------

Pour rappel, Moodle permet de restreindre une activité à un ou plusieurs groupes dans le cours.
Dans ce cas l'activité ne sera visible qu'à ceux-ci.

Le formulaire de création d'activité a une seconde page, obligatoire.

1. Choix de portée de la mise en équipe entre
    - 1a. pour tous les inscrits
    - 1b. segmentée selon le groupe des inscrits
2. Formulaire pour décrire le "teaming", dont la portée est 1a ou 1b.

Si (1b), donc segmentation par groupe, l'enseignant a ensuite la possibilité
de spécifier un teaming différent pour un groupe donné.


Groupes M → classes L
---------------------

La synchronisation est à sens unique.

Les classes sont en lecture seule : l'enseignant ne peut ni modifier leurs attributs, ni les membres.


Synchronisations
----------------

Les synchros ne concernent que les "teamings", et les "groupes" concernés (groupes réels ou tous les inscrits).

### Automatique, lors d'une modification d'activité

À la soumission d'un formulaire d'activité, créer/modifier les classes correspondantes dans LnB :

- soit une classe contenant tous les inscrits au cours (cas 1a ci-dessus),
- soit une classe pour chaque groupe plus une classe hors-groupe.

Ces classes peuvent être vides.

Les étudiants sont synchronisés ensuite (sans suppression),
et inscrits dans les classes de LnB.

 
### Automatique, lors d'un accès à un rapport existant

Si l'étudiant qui consulte l'activité n'a pas de rapport dans LnB (interrogation de l'api LnB),
alors Moodle lance une synchro "groupe → classe" concernant l'étudiant
("groupe" qui peut être en fait tous les inscrits de la classe, cf 1a ci-dessus).
Moodle interroge ensuite l'API de LnB pour demander la création automatique d'un rapport
(si le "teaming" le permet, y compris dans les paramètres de tailles d'équipes).
Si la création auto n'est pas possible, on prévient étudiant et enseignant.

### Automatique, à la connexion d'un enseignant

Récupération uniquement des classes.

### À la demande, par l'enseignant

Au dessus de la liste des membres d'une classe Moodle,
afficher la date de dernière synchro,
et un bouton de nouvelle synchro.

