How to upgrade TinyMCE for LabNbook
===================================

## Tinymce 4.x

1. Prepare a tinymce instance as described in <https://github.com/tinymce/tinymce>.

   It means `grunt` must be installed and `grunt bundle` must be successful.

   I will suppose the source directory is located under the root of LabNbook in `tmp/tinymce/`.
   The commands applied should be similar to:
   ```
   git clone https://github.com/tinymce/tinymce.git tmp/tinymce
   cd tmp/tinymce
   git co 4.9.3
   npm i -g grunt-cli
   npm install
   grunt bundle
   ```

2. Add the external extensions required by LabNbook.

   Copy the `mathmlelements` directory:
   ```
   cp ../../libraries/tinymce/plugins/mathmlelements js/tinymce/plugins/
   ```

   Copy the `epsilon` plugin:
   ```
   mkdir js/tinymce/plugins/epsilon
   cp ../../libraries/epsilonwriter/epsiwriterplugin/epsiwriterplugin.nocache.js js/tinymce/plugins/epsilon/plugin.js
   ```

3. Modify the `epsilon` plugin to load the button image from the local server:

    ```
	<   image: 'https://www.epsilonwriter.com/epsi4/plugin_tinymce/eqneditor.png',
    ---
    >   image: '/libraries/epsilonwriter/eqneditor.png',
	```

4. Minify the "epsilon" plugin, or copy it.

   ```
   minify js/tinymce/plugins/epsilon/plugin.js > js/tinymce/plugins/epsilon/plugin.min.js
   ```

5. Build the TinyMCE bundle that contains all the useful plugins.

   ```
   grunt bundle:minified --themes=modern --plugins=charmap,image,link,lists,paste,table,textcolor,wordcount,mathmlelements,epsilon
   ```

6. Copy the bundle into labnbook's path.

   ```
   cp js/tinymce/tinymce.full.min.js ../../libraries/tinymce/
   ```

## Tinymce 5.x

The compilation process has changed for tinymce 5.x

1. Preparer the environment

        git clone https://github.com/tinymce/tinymce
        # optionnal, to run yarn commands in a working environment :
        docker run –rm -it -v $PWD/tinymce:/target node /bin/bash
        # If you already have a yarn environment working :
        cd tinymce
        # In both cases :
        yarn

2. edit `modules/tinymce/Gruntfile.js` to add modules `mathmlelements` and `epsilon` to the `plugins` list

3. Prepare mathmlelements
 
        mkdir -p modules/tinymce/lib/plugins/mathmlelement/main/ts
        cp ../LabNbook/public/libraries/tinymce/plugins/mathmlelements  modules/tinymce/lib/plugins/mathmlelement/main/ts/Main.js

4. Prepare epsilon

        cp ../LabNbook/public/libraries/epsilonwriter/epsiwriterplugin/epsiwriterplugin.nocache.js modules/tinymce/lib/plugins/epsilon/main/ts/Main.js

5. build

        yarn build

6. publish

    rm -rf ../LabNbook/public/libraries/tinymce/*
    cp -r js/tinymce/* ../LabNbook/public/libraries/tinymce/

7. Download the translation zip from [here](https://www.tiny.cloud/get-tiny/language-packages/) and extract the langs :

        cd ../Labnbook/public/libraries/tinymce/
        unzip /path/to/downloaded.zip
